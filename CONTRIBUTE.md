# Welcome

Don't hesitate to participate!


# Installation

Install required dependancies using composer.
```
composer install
```

# Coding standard

* We use PSR-1 coding convention. Please check your code using a code analyzer.
```
./vendor/bin/phpcs --colors --encoding=utf-8 --standard=PSR1 app/lib/
```

* Please also add and pass unit test, and acceptance tests. We use codeception for that.
```
codecept.phar build
codecept.phar generate:scenarios acceptance
codecept.phar run unit
codecept.phar run acceptance
```

As we use Hyperfom, it need [PhantomJS >= 2.5.0](https://bitbucket.org/ariya/phantomjs/downloads/) (which is beta at the moment) bucause of [missing feature in current stable](https://github.com/ariya/phantomjs/issues/14506)