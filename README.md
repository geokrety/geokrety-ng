# About

GeoKrety (gr. geo = ‘earth’, krety pol. = ‘moles’) is a service similar to TravelBug(TM) or GeoLutins, aiming at tracking objects you leave in geocache containers. People move these registered objects (called GeoKrety) from cache to cache and register each move with this service. The trip of each GeoKret is then presented on a map.

[![Crowdin](https://d322cqt584bo4o.cloudfront.net/geokrety/localized.svg)](https://crowdin.com/project/geokrety)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/geokrety/geokrety-ng/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/geokrety/geokrety-ng/?branch=master)


# Known Issues

* We need a fix for f3-cortex bug [#45](https://github.com/ikkez/f3-cortex/issues/45) (at least it must be applied manually)
