-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Mar 10, 2017 at 09:29 PM
-- Server version: 10.1.13-MariaDB-1~jessie
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `geokrety`
--

--
-- Dumping data for table `gk-waypointy`
--

INSERT INTO `gk-waypointy` (`waypoint`, `lat`, `lon`, `alt`, `country`, `name`, `owner`, `typ`, `kraj`, `link`, `status`, `timestamp`) VALUES
('OC1234', 43.69921, 6.84946, -32768, 'fr', 'test cache', 'kumy', 'Quiz', 'kraj?', 'kumy.net', 1, '2017-01-07 20:35:10'),
('OC1235', 43.69921, 6.84946, -32768, 'fr', 'test cache2', 'kumy', 'Quiz', 'kraj?', 'kumy.net', 1, '2017-01-07 20:35:10'),
('OC1236', 43.69921, 6.84946, -32768, 'fr', 'the test cache', 'kumy', 'Quiz', 'kraj?', 'kumy.net', 1, '2017-01-07 20:35:10'),
('OC1237', 43.69921, 6.84946, -32768, 'fr', 'pas de rapport', 'kumy', 'Quiz', 'kraj?', 'kumy.net', 1, '2017-01-07 20:35:10');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
