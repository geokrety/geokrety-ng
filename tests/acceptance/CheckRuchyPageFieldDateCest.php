<?php


class CheckRuchyPageFieldDateCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @dataprovider dateValidProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkDateValid(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->wantTo('Check if date field works - Valid');
        $I->scrollTo('#date');
        $I->fillField('date', $example[0]);
        $I->canSeeInField('date', $example[0]);
        $I->seeElement(['xpath' => '//input[@id="date"]/../../..[contains(@class, "has-success")]']);
    }

    protected function dateValidProvider() {
        return [
            ['2017-03-11'],
        ];
    }

    /**
     * @dataprovider dateInvalidProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkDateInvalid(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->wantTo('Check if date field works - Invalid');
        $I->scrollTo('#date');
        $I->fillField('date', $example[0]);
        $I->canSeeInField('date', $example[0]);
        $I->waitForText('Please match the requested format.', 3);
        $I->seeElement(['xpath' => '//input[@id="date"]/../../..[contains(@class, "has-error")]']);
        $I->seeElement(['xpath' => '//input[@id="date"]/../../..[contains(@class, "has-danger")]']);
    }

    protected function dateInvalidProvider() {
        return [
            ['0'],
            ['0000-00-00'],
            ['0000-00-00-00'],
            ['00000000'],
            ['2017/03/17'],
            ['2017-17-03'],
            ['17-03-11'],
        ];
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkDateInTheFuture(\AcceptanceTester $I) {
        $I->wantTo('Check if date field works - Future');
        $I->scrollTo('#date');

        $tomorow = date('Y-m-d', strtotime('+1 day'));
        $I->submitForm('#formruchy', [
            'date' => $tomorow
        ]);
        $I->wait(1);
        $I->scrollTo('#date');
        $I->waitForText('The date is from the future', 3);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkMoveDateAlreadyExists(\AcceptanceTester $I) {
        $I->wantTo('Check exactly same move date is rejected');
        $gkid = $I->mockGeoKret('ABCDEF');
        $now = time();
        $curdate = date('Y-m-d', $now);
        $curtime = date('H:i', $now);
        $I->mockMove($gkid, $curdate.' '.$curtime.':00');
        $I->scrollTo('#date');

        $I->submitForm('#formruchy', [
            'nr' => 'ABCDEF',
            'date' => $curdate,
            'time' => $curtime,
            'latlon' => '52.1534 21.0539',
            'username' => 'kumy',
            'captcha' => 'MOCKED',
        ]);
        $I->wait(1);
        $I->waitForText('There is an entry with this date. Correct the date or the hour.', 3);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkMoveDateCommentAlreadyExists(\AcceptanceTester $I) {
        $I->wantTo('Check exactly same move date and comment is rejected');
        $gkid = $I->mockGeoKret('ABCDEF');
        $now = time();
        $curdate = date('Y-m-d', $now);
        $curtime = date('H:i', $now);
        $I->mockMove($gkid, $curdate.' '.$curtime.':00');
        $I->scrollTo('#date');

        $I->submitForm('#formruchy', [
            'nr' => 'ABCDEF',
            'date' => $curdate,
            'time' => $curtime,
            'latlon' => '52.1534 21.0539',
            'username' => 'kumy',
            'captcha' => 'MOCKED',
            'comment' => 'A comment',
        ]);
        $I->wait(1);
        $I->waitForText('Identical log has been submited.', 3);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkMoveDateBeforeGeoKretBorn(\AcceptanceTester $I) {
        $I->wantTo('Check date before GeoKret born');
        $I->mockGeoKret('ABCDEF', 'ABCDEF', '2017-03-11 12:00:00');
        $I->scrollTo('#date');

        $I->submitForm('#formruchy', [
            'nr' => 'ABCDEF',
            'date' => '2017-03-10',
            'time' => '12:00',
            'latlon' => '52.1534 21.0539',
            'username' => 'kumy',
            'captcha' => 'MOCKED',
        ]);
        $I->wait(1);
        $I->waitForText('The date is from the past, BEFORE the GeoKret was created.', 3);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkMoveDateSameAsGeoKretBorn(\AcceptanceTester $I) {
        $I->wantTo('Check date is GeoKret born');
        $gkid = $I->mockGeoKret('ABCDEF', 'ABCDEF', '2017-03-11 12:00:00');

        $I->submitForm('#formruchy', [
            'nr' => 'ABCDEF',
            'date' => '2017-03-11',
            'time' => '12:00',
            'latlon' => '52.1534 21.0539',
            'username' => 'kumy',
            'captcha' => 'MOCKED',
        ]);
        $I->wait(1);
        $I->waitForText('New move successfully registered.', 3);
        $I->seeCurrentUrlEquals('/konkret/'.$gkid);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkTwoDaysAgoButton(\AcceptanceTester $I) {
        $I->wantTo('Check Two days ago button');
        $I->click(['id' => 'date_two_days_ago']);
        $I->canSeeInField(['id' => 'date'], date('Y-m-d', strtotime('-2 day')));
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkYesterdayButton(\AcceptanceTester $I) {
        $I->wantTo('Check Yesterday button');
        $I->click(['id' => 'date_yesterday']);
        $I->canSeeInField(['id' => 'date'], date('Y-m-d', strtotime('-1 day')));
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkTodayButton(\AcceptanceTester $I) {
        $I->wantTo('Check Yesterday button');
        $I->fillField(['id' => 'date'], date('Y-m-d', strtotime('-1 day')));
        $I->click(['id' => 'date_today']);
        $I->canSeeInField(['id' => 'date'], date('Y-m-d'));
    }
}
