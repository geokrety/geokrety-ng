<?php


class CheckRuchyPageFieldLogTypeCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @param AcceptanceTester $I
     * @skip
     */
    public function checkLogTypeAnonymous(\AcceptanceTester $I) {
        $I->wantTo('Check List of Log Types - anonymous');
        // TODO this must be implemented. ref: \App\Lib\Controller\MoveCreate
        // Archive must be absent
        $I->seeNumberOfElements('#logtype option', 5);
    }

    /**
     * @param AcceptanceTester $I
     * @skip
     */
    public function checkLogTypeAuthenticated(\AcceptanceTester $I) {
        $I->am('anonymous');
        $I->wantTo('Check List of Log Types - authenticated');
        $I->mockUser('kumy');
        $I->login('kumy', 'sdfsdf');
        $I->am('registered');
        $I->amOnPage('/ruchy');
        // TODO this must be implemented. ref: \App\Lib\Controller\MoveCreate
        // Archive must be absent
        $I->seeNumberOfElements('#logtype option', 5);
    }

    /**
     * @param AcceptanceTester $I
     * @depends checkLogTypeAnonymous
     */
    public function checkLogTypeArchiveAnonymous(\AcceptanceTester $I) {
        $I->wantTo('Check List of Log Types Archive missing - anonymous');
        // TODO
    }

    /**
     * @param AcceptanceTester $I
     * @depends checkLogTypeAuthenticated
     */
    public function checkLogTypeArchiveAuthenticated(\AcceptanceTester $I) {
        $I->wantTo('Check List of Log Types Archive missing - authenticated');
        // TODO
    }

    /**
     * @param AcceptanceTester $I
     * @depends checkLogTypeAuthenticated
     */
    public function checkLogTypeArchiveAuthenticatedArchiveLink(\AcceptanceTester $I) {
        $I->wantTo('Check List of Log Types Archive present - authenticated');
        // TODO
    }
}
