<?php


class CheckRuchyPageFieldCaptchaCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkCaptchaValid(\AcceptanceTester $I) {
        $I->wantTo('Check if captcha works - Valid');
        $I->submitForm('#formruchy', ['captcha' => 'MOCKED']);
        $I->wait(1);
        $I->seeElement(['xpath' => '//input[@id="captcha"]/../..[contains(@class, "has-success")]']);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkCaptchaInvalid(\AcceptanceTester $I) {
        $I->wantTo('Check if captcha works - Invalid');
        $I->submitForm('#formruchy', ['captcha' => 'BAD VALUE']);
        $I->wait(1);
        $I->seeElement(['xpath' => '//input[@id="captcha"]/../..[contains(@class, "has-error")]']);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkCaptchaCaseInsensitive(\AcceptanceTester $I) {
        $I->wantTo('Check if captcha works - Case Insensitive');
        $I->submitForm('#formruchy', ['captcha' => 'MoCkEd']);
        $I->wait(1);
        $I->seeElement(['xpath' => '//input[@id="captcha"]/../..[contains(@class, "has-success")]']);
    }
}
