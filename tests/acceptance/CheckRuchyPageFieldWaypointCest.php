<?php


class CheckRuchyPageFieldWaypointCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @dataprovider shortWaypointProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkWaypointTooShort(\AcceptanceTester $I, \Codeception\Example $example) {
        $I->wantTo('Check if too short waypoint is detected');
        $I->fillField('wpt', $example[0]);
        $I->canSeeInField('wpt', $example[0]);
        $I->waitForText('Not long enough', 5);
        $I->seeElement(['xpath' => '//input[@id="wpt"]/../..[contains(@class, "has-error")]']);
        $I->seeElement(['xpath' => '//input[@id="wpt"]/../..[contains(@class, "has-danger")]']);
    }

    protected function  shortWaypointProvider() {
        return [
            ['G'],
            ['GC'],
            ['GC5'],
            ['GC5B'],
            ['GC5BR'],
        ];
    }

    /**
     * @dataprovider longWaypointProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkWaypointTooLong(\AcceptanceTester $I, \Codeception\Example $example) {
        $I->wantTo('Check if too long waypoint is detected');
        $I->fillField('wpt', $example[0]);
        $I->canSeeInField('wpt', substr($example[0], 0, 8));
        $I->seeElement(['xpath' => '//input[@id="wpt"]/../..[contains(@class, "has-success")]']);
    }

    protected function  longWaypointProvider() {
        return [
            ['GC5BRQK12'],
            ['         '],
        ];
    }

    /**
     * @skip
     * @param AcceptanceTester $I
     */
    public function checkGoodWaypointFillCoordinates(\AcceptanceTester $I) {
    }
}
