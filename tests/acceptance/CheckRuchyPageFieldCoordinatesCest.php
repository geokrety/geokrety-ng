<?php


class CheckRuchyPageFieldCoordinatesCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @dataprovider coordinatesInvalidProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkCoordinatesInvalid(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->wantTo('Check if Coordinate field works - Invalid');
        $I->scrollTo('#latlon');
        $I->fillField('latlon', $example[0]);
        $I->canSeeInField('latlon', $example[0]);
        $I->waitForText('Not Found', 3); // Better wording should be "Invalid coordinates"
        $I->seeElement(['xpath' => '//input[@id="latlon"]/../..[contains(@class, "has-error")]']);
        $I->seeElement(['xpath' => '//input[@id="latlon"]/../..[contains(@class, "has-danger")]']);
        // TODO mini map should _not_ change
    }

    protected function coordinatesInvalidProvider() {
        return [
            [' '],
            ['foo'],
            ['unkown'],
            ['1'],
        ];
    }

    /**
     * @dataprovider coordinatesValidProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkCoordinatesValid(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->wantTo('Check if Coordinate field works - Valid');
        $I->scrollTo('#latlon');
        $I->fillField('latlon', $example[0]);
        $I->canSeeInField('latlon', $example[0]);
        $I->seeElement(['xpath' => '//input[@id="latlon"]/../..[contains(@class, "has-success")]']);
        // TODO mini map should change
    }

    protected function coordinatesValidProvider() {
        return [
            ['1 1'],
            [' 1 1 '],
            ['52.1534 21.0539'],
            ['N 52° 09.204 E 021° 03.234'],
            ['N 52° 9\' 12.2400" E 21° 3\' 14.0400"'],
        ];
    }
}
