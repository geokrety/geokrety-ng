<?php


class CheckRuchyPageFieldCommentCest {
    const MAX_LENGTH = 5120;

    const CSS_NORMAL = 0;
    const CSS_SUCCESS = 1;
    const CSS_ERROR = 2;

    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @dataprovider commentLengthProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkCommentLength(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->wantTo('Check if comment field works');
        $I->scrollTo(['id' => 'comment']);
        $I->fillField(['id' => 'comment'], $example[0]);
        $I->canSeeInField(['id' => 'comment'], $example[0]);
        switch ($example[1]) {
            case $this::CSS_NORMAL:
                $I->seeElement(['xpath' => '//textarea[@id="comment"]/../..[not(contains(@class, "has-success"))]']);
                break;
            case $this::CSS_SUCCESS:
                $I->seeElement(['xpath' => '//textarea[@id="comment"]/../..[contains(@class, "has-success")]']);
                break;
            case $this::CSS_ERROR:
                $I->seeElement(['xpath' => '//textarea[@id="comment"]/../..[contains(@class, "has-error")]']);
                break;
        }
        $expectedlength = $this::MAX_LENGTH - strlen($example[0]);
        $I->waitForText($expectedlength.' characters left.', 3);
    }

    protected function commentLengthProvider() {
        return [
            // string, expected ccs
            ['', $this::CSS_NORMAL],
            ['2', $this::CSS_SUCCESS],
            ['some nice long text...', $this::CSS_SUCCESS],
            //[str_repeat('0', 5120), $this::CSS_SUCCESS],
            //[str_repeat('0', 5121), $this::CSS_ERROR],
        ];
    }
}
