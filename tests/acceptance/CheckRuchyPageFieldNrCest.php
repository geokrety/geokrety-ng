<?php


class CheckRuchyPageFieldNrCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    public function checkNrTooShort(\AcceptanceTester $I) {
        $I->wantTo('Check if too short NR is detected');
        $I->fillField('nr', 'XF3');
        $I->canSeeInField('nr', 'XF3');
        $I->waitForText('Not long enough', 3);
        $I->seeElement(['xpath' => '//input[@id="nr"]/../..[contains(@class, "has-error")]']);
        $I->seeElement(['xpath' => '//input[@id="nr"]/../..[contains(@class, "has-danger")]']);
        $I->submitForm('#formruchy', []);
        $I->wait(1);
        $I->see('Please fix the errors.');
        $I->see('The Tracking Code field needs to be exactly 6 characters in length');
        $I->see('This Tracking Code is invalid');
    }

    public function checkNrTooLong(\AcceptanceTester $I) {
        $I->wantTo('Check if too long NR is detected');
        $I->fillField('nr', 'XF3XF3X');
        $I->canSeeInField('nr', 'XF3XF3');
    }

    public function checkNrGoodLenght(\AcceptanceTester $I) {
        $I->wantTo('Check if NR with good lenght is detected');
        $I->fillField('nr', 'XF3ACC');
        $I->waitForText('Not Found', 3);
        $I->submitForm('#formruchy', []);
        $I->wait(1);
        $I->see('Please fix the errors.');
        $I->see('This Tracking Code is invalid');
        $I->dontSee('The Tracking Code field needs to be exactly 6 characters in length');
    }

    public function checkNrValid(\AcceptanceTester $I) {
        $I->wantTo('Check if valid NR detected');
        $I->mockGeoKret('ABCDEF');
        $I->fillField('nr', 'ABCDEF');
        $I->wait(1);
        // https://github.com/1000hz/bootstrap-validator/issues/473
        //$I->waitForText('This Tracking Code is invalid', 30);
        $I->seeElement(['xpath' => '//input[@id="nr"]/../..[contains(@class, "has-success")]']);
        //$I->seeElement(['xpath' => '//*[contains(@class, \'has-success\')]']);
        $I->submitForm('#formruchy', []);
        $I->wait(1);
        $I->see('Please fix the errors.');
        $I->dontSee('This Tracking Code is invalid');
        $I->dontSee('The Tracking Code field needs to be exactly 6 characters in length');
    }
}
