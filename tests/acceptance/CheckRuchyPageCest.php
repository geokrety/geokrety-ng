<?php

class CheckRuchyPageCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    public function checkLoadedPageAnonymous(\AcceptanceTester $I) {
        $I->am('anonymous');
        $I->wantTo('Check page was loaded, with all categories - anonymous');
        $I->see('However it is possible to perform operations on GeoKrety without logging in, we encourage you to create an account and log in. It will take you about 15 seconds :). ');
        $I->see('Choose log type');
        $I->see('Identify GeoKret');
        $I->see('New location');
        $I->see('Captcha verification');
        $I->see('Additional data');
    }

    public function checkLoadedPageAuthenticated(\AcceptanceTester $I) {
        $I->am('anonymous');
        $I->wantTo('Check page was loaded, with all categories - registered');
        $I->mockUser('kumy');
        $I->login('kumy', 'sdfsdf');
        $I->am('registered');
        $I->amOnPage('/ruchy');
        $I->dontSee('However it is possible to perform operations on GeoKrety without logging in, we encourage you to create an account and log in. It will take you about 15 seconds :). ');
        $I->see('Choose log type');
        $I->see('Identify GeoKret');
        $I->see('New location');
        $I->dontSee('Captcha verification');
        $I->see('Additional data');
    }

    public function checkFieldLogtypeContent(\AcceptanceTester $I) {
        $I->wantTo('Check logtype filled');
        $I->seeInFormFields('#formruchy', ['logtype' => 0, // 'I\'ve dropped GeoKret'
            'nr' => '',
            'captcha' => '',]);
    }

    public function checkSubmitEmptyReturnError(\AcceptanceTester $I) {
        $I->wantTo('Check if empty form return an error');
        $I->submitForm('#formruchy', []);
        $I->wait(1);
        $I->see('New location');
        $I->see('Please fix the errors.');
        $I->expect('the form is not submitted');
    }
}