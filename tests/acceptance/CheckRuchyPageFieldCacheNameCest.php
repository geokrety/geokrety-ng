<?php


class CheckRuchyPageFieldCacheNameCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @dataprovider cacheNameProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkCacheNameKnown(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->mockWaypointOC();
        $I->mockWaypointOC('OC1234', 'test');
        $I->mockWaypointOC('OC1235', 'Foo Bar');
        $I->mockWaypointOC('OC1236', 'test cache');
        $I->mockWaypointOC('OC1237', 'fake test cache');
        $I->mockWaypointOC('OC1238', 'hello world');
        $I->mockWaypointOC('OC1239', 'Bar bar');
        $I->mockWaypointOC('OC1240', 'BaR bAr TeSt');
        $I->mockWaypointOC('OC1241', 'fake hello');
        $I->mockWaypointOC('OC1242', 'Hello');
        $I->mockWaypointOC('OC1243', '');

        $I->wantTo('Check if Autocompletion works');
        $I->scrollTo('#name_of_cache');
        $I->fillField('name_of_cache', $example[0]);
        $I->canSeeInField('name_of_cache', $example[0]);
        if ($example[1] > 0) {
            $I->waitForElement('#eac-container-name_of_cache ul li', 5);
        }
        $I->canSeeNumberOfElements('#eac-container-name_of_cache ul li', $example[1]);
    }

    protected function cacheNameProvider() {
        return [
            ['foo', 0],
            ['ba', 0],
            ['d', 0],
            ['', 0],
            ['test', 4],
            ['fake', 2],
            ['hello', 3],
            ['WORLD', 1],
            ['unkown cache name', 0],
            ['unkown', 0],
        ];
    }

    /**
     * @skip
     * @param AcceptanceTester $I
     */
    public function checkGoodCacheNameFillCoordinates(\AcceptanceTester $I) {
    }
}
