<?php


class CheckRuchyPageFieldUserNameCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkUsernameAnonymous(\AcceptanceTester $I) {
        $I->wantTo('Check if username field is available to anonymous users');
        $I->scrollTo('#username');
        $I->seeElement(['xpath' => '//input[@id="username" and not(@readonly)]']);
        $I->fillField('username', 'kumy');
        $I->canSeeInField('username', 'kumy');
        $I->seeElement(['xpath' => '//input[@id="username"]/../..[contains(@class, "has-success")]']);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkUsernameAuthenticated(\AcceptanceTester $I) {
        $I->wantTo('Check if username field is readonly for authenticated users');
        $I->mockUser('kumy');
        $I->login('kumy', 'sdfsdf');
        $I->amOnPage('/ruchy');
        $I->scrollTo('#username');
        $I->canSeeInField('username', 'kumy');
        $I->seeElement(['xpath' => '//input[@id="username" and @readonly]']);
        $I->seeElement(['xpath' => '//input[@id="username"]/../..[contains(@class, "has-success")]']);
    }
}
