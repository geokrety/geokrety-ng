<?php


class CheckRuchyPageFieldTimeCest {
    public function _before(\AcceptanceTester $I) {
        $I->amOnPage('/ruchy');
    }

    public function _after(\AcceptanceTester $I) {
    }

    /**
     * @dataprovider timeValidProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkTimeValid(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->wantTo('Check if time field works - Valid');
        $I->scrollTo('#time');
        $I->fillField('time', $example[0]);
        $I->canSeeInField('time', $example[0]);
        $I->seeElement(['xpath' => '//input[@id="time"]/../..[contains(@class, "has-success")]']);
    }

    protected function timeValidProvider() {
        return [
            ['00:00'],
            ['12:00'],
            ['12:24'],
            ['23:59'],
        ];
    }

    /**
     * @dataprovider timeInvalidProvider
     * @param AcceptanceTester $I
     * @param \Codeception\Example $example
     */
    public function checkTimeInvalid(\AcceptanceTester $I,
        \Codeception\Example $example) {
        $I->wantTo('Check if time field works - Invalid');
        $I->scrollTo('#time');
        $I->fillField('time', $example[0]);
        $I->canSeeInField('time', $example[0]);
        $I->waitForText('Please match the requested type.', 3);
        $I->seeElement(['xpath' => '//input[@id="time"]/../..[contains(@class, "has-error")]']);
        $I->seeElement(['xpath' => '//input[@id="time"]/../..[contains(@class, "has-danger")]']);
    }

    protected function timeInvalidProvider() {
        return [
            ['0'],
            ['0000'],
            ['    '],
            [' '],
            ['24:50'],
            ['08:88'],
            ['12 12'],
            ['hh:mm'],
            ['-8:12'],
            ['8:1'],
        ];
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkTimeInTheFuture(\AcceptanceTester $I) {
        $I->wantTo('Check if date field works - Future');
        $I->scrollTo('#time');

        $today = date('Y-m-d', strtotime('+1 hour'));
        $onehour = date('H:m', strtotime('+1 hour'));
        $I->submitForm('#formruchy', [
            'date' => $today,
            'time' => $onehour,
        ]);
        $I->wait(1);
        $I->waitForText('The date is from the future', 3);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkMoveTimeBeforeGeoKretBorn(\AcceptanceTester $I) {
        $I->wantTo('Check date before GeoKret born');
        $I->mockGeoKret('ABCDEF', 'ABCDEF', '2017-03-11 12:00:00');
        $I->scrollTo('#time');

        $I->submitForm('#formruchy', [
            'nr' => 'ABCDEF',
            'date' => '2017-03-11',
            'time' => '11:00',
            'latlon' => '52.1534 21.0539',
            'username' => 'kumy',
            'captcha' => 'MOCKED',
        ]);
        $I->wait(1);
        $I->waitForText('The date is from the past, BEFORE the GeoKret was created.', 3);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function checkNowButton(\AcceptanceTester $I) {
        $I->wantTo('Check Now button');
        $I->fillField(['id' => 'time'], 'hh:mm');
        $I->click(['id' => 'time_now']);
        $I->canSeeInField(['id' => 'time'], date('H:i'));
    }
}
