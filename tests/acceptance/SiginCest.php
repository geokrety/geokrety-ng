<?php
class SiginCest {
    public function checkLogin(\AcceptanceTester $I) {
        $I->am('anonymous');
        $I->wantTo('login to website');
        $I->lookForwardTo('access all website features');
        $I->mockUser('kumy');
        $I->login('kumy', 'sdfsdf');
        $I->am('user');
        $I->see('Welcome on board.');
        $I->expect('User logged in');

        $I->wantTo('logout from website');
        $I->amOnPage('/logout');
        $I->see('You\'ve been logged out.');
        $I->expect('User logged out');
    }
}
