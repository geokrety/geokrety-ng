<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor {
    use _generated\AcceptanceTesterActions;

    /**
     * Login function
     * @param $name
     * @param $password
     */

    public function login($name, $password) {
        $I = $this;

        //        // if snapshot exists - skipping login
        //        if ($I->loadSessionSnapshot('login')) {
        //            return;
        //        }

        // logging in
        $I->amOnPage('/login');
        $I->submitForm('#loginForm', [
            'login' => $name,
            'haslo1' => $password
        ]);
        $I->see('Welcome on board.', '.alert');

        //        // saving snapshot
        //        $I->saveSessionSnapshot('login');
    }

    /**
     * Mock a user.
     * Default password is 'sdfsdf'
     *
     * @param string|null $username Username
     * @return int userid
     */
    public function mockUser($username = NULL) {
        return $this->haveInDatabase('gk-users', [
                'user' => is_null($username)?'kumy':$username,
                'haslo2' => '$2a$11$.vK56KyusPG/n6OVjxCMXue8oYxcfYSDtdU21y1fhCdMeTqSBUrLy',
                'email' => 'none@foo.bar',
                'ip' => '127.0.0.1',
                'godzina' => 0,
                'ostatni_login' => '2016-12-18 21:12:17',
                'secid' => ''
            ]
        );
    }

    public function mockGeoKret($nr = NULL, $id = NULL, $creationDate = NULL) {
        $payload = [
            'nr' => is_null($nr)?'ABCDEF':$nr,
            'droga' => 0,
            'skrzynki' => 0,
            'ost_pozycja_id' => 0,
            'ost_log_id' => 0,
            'typ' => '0',
            'avatarid' => 0,
            'data' => is_null($creationDate)?'2016-12-18 21:12:17':$creationDate,
            'timestamp_oc' => '2016-12-18 21:12:17'
        ];
        if (!is_null($id)) {
            $payload['id'] = $id;
        }
        return $this->haveInDatabase('gk-geokrety', $payload);
    }

    public function mockWaypointGC($wpt = NULL,
        $lat = NULL, $lon = NULL,
        $alt = NULL, $country = NULL) {

        return $this->haveInDatabase('gk-waypointy-gc', [
                'wpt' => is_null($wpt)?'GC5BRQK':$wpt,
                'lat' => is_null($lat)?43.69365:$lat,
                'lon' => is_null($lon)?6.86097:$lon,
                'alt' => is_null($alt)?759:$alt,
                'country' => is_null($country)?'fr':$country,
            ]
        );
    }

    public function mockWaypointOC($wpt = NULL, $name = NULL,
        $lat = NULL, $lon = NULL,
        $alt = NULL, $country = NULL,
        $owner = NULL, $typ = NULL,
        $kraj = NULL, $link = NULL, $status = NULL,
        $timestamp = NULL) {

        return $this->haveInDatabase('gk-waypointy', [
                'waypoint' => is_null($wpt)?'OC5BRQK':$wpt,
                'lat' => is_null($lat)?43.69365:$lat,
                'lon' => is_null($lon)?6.86097:$lon,
                'alt' => is_null($alt)?759:$alt,
                'country' => is_null($country)?'fr':$country,
                'name' => is_null($name)?'Cache name':$name,
                'owner' => is_null($owner)?'kumy':$owner,
                'typ' => is_null($typ)?'Quiz':$typ,
                'kraj' => is_null($kraj)?'kraj':$kraj,
                'link' => is_null($link)?'geokrety.org':$link,
                'status' => is_null($status)?'1':$status,
                'timestamp' => is_null($timestamp)?'2017-01-07 20:35:10':$timestamp,
            ]
        );
    }

    public function mockMove($gkid = NULL, $datetime = NULL, $comment=NULL) {
        return $this->haveInDatabase('gk-ruchy', [
                'id' => is_null($gkid)?'1':$gkid,
                'data' => is_null($datetime)?date('Y-m-d H:i:s'):$datetime,
                'koment' => is_null($comment)?'A comment':$comment,
            ]
        );
    }
}
