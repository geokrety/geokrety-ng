<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

   /**
    * Define custom actions here
    */
   public function loadF3() {
       chdir(__DIR__ . '/../../app');

       // Kickstart the framework
       $f3 = \Base::instance();

       // Initialize CMS
       $f3->config('config/default.ini');

       // Initialise with custom values
       $f3->config('config/config.ini');

       // Define routes
       $f3->config('config/routes.ini');
   }
}
