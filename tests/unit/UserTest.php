<?php


class UserTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $db;

    protected function _before() {
        $dbc = \Codeception\Configuration::suiteSettings('unit', \Codeception\Configuration::config());

        $this->db = new \DB\SQL(
            $dbc['modules']['config']['Db']['dsn'],
            $dbc['modules']['config']['Db']['user'],
            $dbc['modules']['config']['Db']['password'],
            array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
        );
    }

    protected function _after() {
    }

    // tests
    public function testCreate() {
        $this->tester->dontSeeInDatabase('gk-users', ['user' => 'testuser']);

        $user = new \App\Models\User();
        $user->user = 'testuser';
        $user->hashPassword('sdf123!');
        $user->save();

        $this->tester->seeInDatabase('gk-users', ['user' => 'testuser']);

        return $user;
    }

    /**
     * @depends testCreate
     */
    public function testGetById() {
        $user = new \App\Models\User();
        $user->user = 'testuser';
        $user->save();

        $user->getById(0);
        $this->assertTrue($user->dry());

        $user->getById('');
        $this->assertTrue($user->dry());

        $user->getById('0');
        $this->assertTrue($user->dry());

        $user->getById(1);
        $this->assertFalse($user->dry());
        $this->assertEquals('testuser', $user->user);

        $user->getById('1');
        $this->assertFalse($user->dry());
        $this->assertEquals('testuser', $user->user);

        $user->getById(2);
        $this->assertTrue($user->dry());


    }

    /**
     * @depends testCreate
     */
    public function testGetByName() {
        $user = new \App\Models\User();
        $user->user = 'testuser';
        $user->save();

        $user->getByName('testuser');
        $this->assertFalse($user->dry());
        $this->assertEquals('testuser', $user->user);

        $user->getByName('bob');
        $this->assertTrue($user->dry());

        $user->getByName('0');
        $this->assertTrue($user->dry());

        $user->getByName(1);
        $this->assertTrue($user->dry());

        $user->getByName(0);
        $this->assertTrue($user->dry());

        $user->getByName('%');
        $this->assertTrue($user->dry());
    }

    /**
     * @depends testCreate
     */
    public function testGetByEmail() {
        $user = new \App\Models\User();
        $user->user = 'testuser';
        $user->email = 'foo@bar.net';
        $user->save();

        $user->getByEmail('foo@bar.net');
        $this->assertFalse($user->dry());
        $this->assertEquals('testuser', $user->user);

        $user->getByEmail('bob@bob.net');
        $this->assertTrue($user->dry());

        $user->getByEmail('bob');
        $this->assertTrue($user->dry());

        $user->getByEmail('0');
        $this->assertTrue($user->dry());

        $user->getByEmail(1);
        $this->assertTrue($user->dry());

        $user->getByEmail(0);
        $this->assertTrue($user->dry());

        $user->getByEmail('%');
        $this->assertTrue($user->dry());
    }
}