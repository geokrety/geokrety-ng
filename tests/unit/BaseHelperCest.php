<?php

/**
 * Class BaseHelperCest
 * @covers \App\Helpers\BaseHelper
 */
class BaseHelperCest {
    /**
     * Call protected/private method of a class.
     *
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    protected function invokeMethod($object, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(TRUE);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * @dataprovider providerTestResolvAtrrRender
     * @covers       \App\Helpers\BaseHelper::resolveAttr
     * @param UnitTester $t
     * @param \Codeception\Example $example
     */
    public function testResolveAttrRender(UnitTester $t, \Codeception\Example $example) {
        $t->assertSame($example[3],
            $this->invokeMethod(
                new \App\Helpers\BaseHelper(), 'resolveAttr',
                array($example[0], $example[1], $example[2])));
    }

    protected function providerTestResolvAtrrRender() {
        return array(
            array([''], 0, '', "''"),
            array([''], '', '', "''"),
            array([''], '', 123, 123),
            array([''], '', '123', "'123'"),
            array([], 0, 'default', "'default'"),
            array([], 0, 23, 23),
            array(['123'], 0, 'default', "'123'"),
            array(['123'], 0, '', "'123'"),
            array(["{{'123'}}"], 0, '456', "'123'"),
            array(["{{'@var1'}}"], 0, '456', '\'$var1\''),
            array(['{{@var1}}'], 0, '456', '$var1'),
            array([], 0, 0, 0),
            array([NULL], 0, 1, 1),
            array([], 0, NULL, "''"),
            array([NULL], 0, NULL, "''"),
            array([NULL], 0, 'abc', "'abc'"),
            array(NULL, 0, NULL, "''"),

            array(NULL, NULL, NULL, "''"),
            array([], NULL, NULL, "''"),
            array(['123'], NULL, NULL, "''"),
            array([123], NULL, NULL, "''"),

            array([0, 1, 2, 3], 1, 'abc', 1),
            array([0, 1, 2, 3], 2, 'abc', 2),
            array([0, 1, 2, 3], 666, 'abc', "'abc'"),
            array([0, 1, 2, 3], 'abc', 'abc', "'abc'"),

            array('', 0, 'abc', "'abc'"),
            array(['0', '1', '2', '3'], 666, 'abc', "'abc'"),
            array(['0', '1', '2', '3'], 'abc', 'abc', "'abc'"),
            array(['0', '1', '2', '3'], 1, 'abc', "'1'"),

            array(['', 1, 2, 3], 0, 'abc', "''"),
            array([0, 1, 2, 3], 0, 'abc', 0),
            array(['0', '1', '2', '3'], 0, 'abc', "'0'"),
            array(['0', '1', '2', '3'], '666', 'abc', "'abc'"),
        );
    }

    /**
     * @dataprovider providerTestTranslate
     * @param UnitTester $t
     * @param \Codeception\Example $example
     */
    public function testTranslate(UnitTester $t, \Codeception\Example $example) {
        $t->loadF3();
        $t->assertSame($example[1],
            $this->invokeMethod(
                new \App\Helpers\BaseHelper(), 'translate', array($example[0])));
    }

    protected function providerTestTranslate() {
        return array(
            //array('', ''), // Invalid hive key ''
            array('gkt_gktype_human', 'A human'),
            array('gkt_mail_confirm_expired_token', 'This confirmation code has expired.'),
            array('mail_confirm_expired_token', null),
        );
    }
}
