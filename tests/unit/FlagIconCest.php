<?php

class FlagIconCest {
    /**
     * @dataprovider providerTestFlagIconRender
     * @covers \App\Helpers\FlagIcon::render
     * @param UnitTester $t
     * @param \Codeception\Example $example
     * @internal param $input
     * @internal param $output
     */
    public function testFlagIconRender(UnitTester $t, \Codeception\Example $example) {
        $t->assertSame($example[1], \App\Helpers\FlagIcon::render($example[0]));
    }

    protected function providerTestFlagIconRender() {
        return array(
            'empty' => array(array(), ''),

            'country_fr' => array(array('0' => 'FR'),
                "<?php echo \\App\\Helpers\\FlagIcon::instance()->build('FR', 16, 11, 0, '', 'FR', 'FR'); ?>"),

            'country_pl' => array(array('0' => 'PL'),
                "<?php echo \\App\\Helpers\\FlagIcon::instance()->build('PL', 16, 11, 0, '', 'PL', 'PL'); ?>"),

            'one_attrib' => array(array('0' => 'FR',
                '@attrib' => array('border' => 1)),
                "<?php echo \\App\\Helpers\\FlagIcon::instance()->build('FR', 16, 11, 1, '', 'FR', 'FR'); ?>"),

            'full_attrib' => array(array('0' => 'DE',
                '@attrib' => array('width' => 18, 'height' => 17, 'border' => 16,
                    'class' => 'bold clear', 'alt' => 'alt', 'title' => 'title')),
                "<?php echo \\App\\Helpers\\FlagIcon::instance()->build('DE', 18, 17, 16, 'bold clear', 'alt', 'title'); ?>"),
        );
    }

    /**
     * @dataprovider providerTestFlagIconBuild
     * @covers       \App\Helpers\FlagIcon::build
     * @param UnitTester $t
     * @param \Codeception\Example $example
     */
    public function testFlagIconBuild(UnitTester $t, \Codeception\Example $example) {
        $flagicon = new \App\Helpers\FlagIcon();
        $t->assertSame($example[7],
            $flagicon->build($example[0], $example[1], $example[2], $example[3],
                $example[4], $example[5], $example[6]));
    }


    protected function providerTestFlagIconBuild() {
        return array(
            'empty' => array('', '', '', '', '', '', '', ''),
            'country_fr' => array('FR', '', '', '', '', '', '',
                '<img class="" src="'.\App\Helpers\FlagIcon::FLAG_DIR.
                'FR.png" alt="" title="" width="" height="" border="" />'),
            'country_pl' => array('PL', '', '', '', '', '', '',
                '<img class="" src="'.\App\Helpers\FlagIcon::FLAG_DIR.
                'PL.png" alt="" title="" width="" height="" border="" />'),
            'full_attrib' => array('DE', '18', '17', '16', 'bold clear', 'alt', 'title',
                '<img class="bold clear" src="'.\App\Helpers\FlagIcon::FLAG_DIR.
                'DE.png" alt="alt" title="title" width="18" height="17" border="16" />'),
        );
    }
}
