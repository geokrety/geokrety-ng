# Requirements

* php >= 5.6
* composer
* An sql database (ex: Mariadb)

# Production install

* Clone the git repository.
* Install depencies via composer
    composer install --no-dev
