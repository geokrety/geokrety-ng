<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;

/**
 * Class Login
 * @package App\Controllers
 */
class Login extends Controller {

    /**
     * Display the login page.
     *
     * @param \Base $f3
     */
    function display($f3) {
        if (self::isLoggedIn()) {
            Flash::instance()
                ->addMessage($f3->get('gkt_login_you_are_already_connected'), 'success');
            $f3->reroute('@home');
        }

        $f3->set('title', 'Login');
        $f3->set('content', 'login.htm');
        echo \Template::instance()->render('layout.htm');
    }

    /**
     * Authenticate user.
     *
     * @param \Base $f3
     */
    function authenticate($f3) {
        if (self::isLoggedIn()) {
            Flash::instance()
                ->addMessage($f3->get('gkt_login_you_are_already_connected'), 'success');
            $f3->reroute('@home');
        }

        $username = $f3->get('POST.login');
        $password = $f3->get('POST.haslo1');
        $remember = $f3->get('POST.remember');

        // TODO redirect to an eventual url or home

        $user = new \App\Models\User();
        $user->getByName($username);

        // user not found
        if ($user->dry()) {
            Flash::instance()
                ->addMessage($f3->get('gkt_login_invalid_username'), 'danger');
            $this->f3->reroute('@login');
        }

        // Check password
        if (!$user->check_password($password)) {
            Flash::instance()->addMessage($f3->get('gkt_login_invalid_password'), 'danger');
            $this->f3->reroute('@login');
        }

        $f3->set('SESSION.isLoggedIn', TRUE);
        $f3->set('SESSION.userid', $user->userid);
        $f3->set('SESSION.user', $user->user);
        $user->setLastVisit();
        $user->save();

        // keep me logged in a COOKIE
        if (isset($remember)) {
            $f3->set('COOKIE.userid', $user->userid, 86400); // Keep login for 24h
            $f3->set('COOKIE.user', $user->user, 86400); // Keep login for 24h
        }

        Flash::instance()
            ->addMessage($f3->get('gkt_login_welcome_on_board'), 'success');
        $this->f3->reroute('@home');

    }

    /**
     * Display logout page, and logout user.
     *
     * @param \Base $f3
     */
    function logout($f3) {
        $f3->clear('SESSION');
        $f3->clear('COOKIE.userid');
        Flash::instance()
            ->addMessage($f3->get('gkt_login_you_have_been_logged_out'), 'warning');
        $f3->reroute('@home');
    }

}
