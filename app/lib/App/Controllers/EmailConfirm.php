<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;
use \App\Models\ActiveEmails;

/**
 * Class EmailConfirm
 * @package App\Controllers
 */
class EmailConfirm extends Controller {

    /**
     * Process confirmation code.
     *
     * @param \Base $f3
     */
    public function display($f3) {
        // missing
        if ($f3->get('PARAMS.kod') == NULL) {
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_missing_token'), 'danger');
            $f3->reroute('@home');
        }

        $kod = $f3->get('PARAMS.kod');
        $this->processKod($f3, $kod);
    }

    /**
     * Display information about a confirmation code.
     *
     * @param \Base $f3
     */
    public function displayKod($f3) {
        // missing
        if ($f3->get('PARAMS.kod') == NULL) {
            return;
        }

        $kod = $f3->get('PARAMS.kod');

        // empty
        if (empty($kod)) {
            return;
        }

        // decode challenge
        $kodinfo = ActiveEmails::checkVerificationCode($kod);
        if ($kodinfo === FALSE) {
            return;
        }

        echo 'time_of_request: '.date('Y-m-d H:i:s', $kodinfo[1]).'<br/>';
        echo 'requested_by_user: '.$kodinfo[2].'<br/>';
    }

    /**
     * Verify a confirmation code.
     *
     * @param \Base $f3
     * @param string $kod
     */
    private function processKod($f3, $kod) {
        // empty
        if (empty($kod)) {
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_empty_token'), 'danger');
            $f3->reroute('@home');
        }

        // pattern
        if (!preg_match('#^[a-z0-9_\-]{29,37}$#i', $kod)) {
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_malformed_token'), 'danger');
            $f3->reroute('@home');
        }

        // decode challenge
        $kodinfo = ActiveEmails::checkVerificationCode($kod);
        if ($kodinfo === FALSE) {
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_unrecognized_token'), 'danger');
            $f3->reroute('@home');
        }
        $time_of_request = $kodinfo[1];
        $requested_by_user = $kodinfo[2];

        // expiration
        if ($time_of_request + 5 * 24 * 3600 < time()) {
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_expired_token'), 'danger');
            $link = $f3->alias('my_edit_email');
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_to_change_your_email', $link),
                    'warning');
            $f3->reroute('@home');
        }

        // check token in db
        $token = new ActiveEmails();
        $token->getByToken($kod);
        if ($token->dry()) {
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_not_found_token'), 'danger');
            $f3->reroute('@home');
        }

        if ($token->done == ActiveEmails::KOD_CONFIRMED) {
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_token_already_processed'),
                    'warning');
            $f3->reroute('@home');
        }

        try {
            $this->db->begin();

            // Save user
            $user = new \App\Models\User();
            $user->getById($requested_by_user);
            if ($user->dry()) {
                Flash::instance()
                    ->addMessage($f3->get('gkt_mail_confirm_user_not_found'), 'danger');
                $f3->reroute('@home');
            }
            $user->email = $token->email;
            $user->save();

            // Set kod as processed
            $token->done = ActiveEmails::KOD_CONFIRMED;
            $token->save();

            $this->db->commit();
        } catch (\PDOException $e) {
            // Something wrong
            Flash::instance()
                ->addMessage($f3->get('gkt_mail_confirm_token_save_fail'), 'danger');
            \App\ErrorLog::exception($e);
            $f3->reroute('@home');
        }

        Flash::instance()
            ->addMessage($f3->get('gkt_mail_confirm_sucessfully_updated_email'),
                'success');
        $f3->reroute('@home');
    }
}
