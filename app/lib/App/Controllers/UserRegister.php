<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;

/**
 * Class RegisterUser
 * @package App\Controllers
 */
class UserRegister extends Controller {

    /**
     * Display User registration page.
     *
     * @param \Base $f3
     */
    function display($f3) {
        if (self::isLoggedIn()) {
            Flash::instance()->addMessage($f3->get('gkt_page_for_anonymous',
                $f3->alias('logout')), 'warning');
            $f3->reroute('@home');
        }

        // TODO define list of languages

        # Password strength
        $f3->push('javascripts',
            'https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js');

        $f3->copy('gkt_register_register_a_new_user_title', 'title');
        $f3->set('content', 'register_user.htm');
        echo \Template::instance()->render('layout.htm');
    }

    /**
     * Generate a captcha image.
     * TODO move elsewhere, Helpers?
     */
    function captcha() {
        if ($this->f3->get('MOCK_CAPTCHA')) {
            $this->f3->set('SESSION.register_captcha_code', 'MOCKED');
            $captcha = new \Image('/images/mocked-captcha.png');
            $captcha->render();
            return;
        }

        $captcha = new \Image();
        $captcha->captcha('fonts/RybnikRegular.ttf', 16, 6,
            'SESSION.register_captcha_code', '', 0xE9865B, 0x000000);
        $captcha->render();
    }

    /**
     * Verify form values.
     *
     * @param $f3 \Base
     */
    function register_check($f3) {
        if (self::isLoggedIn()) {
            Flash::instance()->addMessage($f3->get('gkt_page_for_anonymous',
                $f3->alias('logout')), 'warning');
            $f3->reroute('@home');
        }
        $validator = new \Validate();

        // Check captcha
        $validator->add_validator('match_captcha',
            function($field, $input, $param = NULL) {
                return $param == strtoupper($input[$field]);
            });

        // Check if email is taken
        $validator->add_validator('user_email_absent',
            function($field, $input, $param = NULL) {
                if (empty($input[$field])) {
                    return TRUE;
                }
                $user = new \App\Models\User();
                $user->getByEmail($input[$field]);
                return $user->dry();
            });

        // Check if username is taken
        $validator->add_validator(
            'user_absent', function($field, $input, $param = NULL) {
            $user = new \App\Models\User();
            $user->getByName($input[$field]);
            if ($user->dry()) {
                return TRUE;
            }
            // Check if user recently registered
            $user->isRecentlyRegistered($input[$field]);
            if ($user->dry()) {
                $f3 = \Base::instance();
                Flash::instance()->addMessage(
                    $f3->get('gkt_register_account_already_created',
                        $f3->alias('user', ['userid' => $user->userid])).
                    '<br /><br />'.
                    $f3->get('gkt_register_an_email_to_confirm_your_email_has_been_sent').
                    '<br /><br />'.
                    '[<a href="'.$f3->alias('login').'">'.$f3->get('gkt_login').'</a>]'.
                    ' '.
                    '[<a href="'.$f3->alias('lost_password').'">'.
                    $f3->get('gkt_forgot_password').'</a>]'
                    , 'warning');
            }
            return FALSE;
        });

        // TODO missing csrf ?
        $validator->validation_rules(array(
            'user' => 'required|alpha_numeric|user_absent',
            'haslo1' => 'required|max_len,80|min_len,5',
            'haslo2' => 'required|max_len,80|min_len,5|equalsfield,haslo1',
            'email' => 'required|valid_email|max_len,150|user_email_absent',
            'captcha' => 'required|match_captcha,'.
                $f3->get('SESSION.register_captcha_code'),
            'lang' => 'required|exact_len,2'
        ));

        $validator->filter_rules(array(
            'user' => 'trim|sanitize_string',
            'email' => 'trim|sanitize_email'
        ));

        $validator->set_error_messages(array(
            'user_absent' => $f3->get('gkt_validate_username_taken'),
            'user_email_absent' => $f3->get('gkt_validate_email_already_in_use'),
            'match_captcha' => $f3->get('gkt_register_captcha_differ'),
        ));

        $validator->set_field_names(array(
            'user' => $f3->get('gkt_register_username'),
            'haslo1' => $f3->get('gkt_register_password'),
            'haslo2' => $f3->get('gkt_register_verify_password'),
            'email' => $f3->get('gkt_register_email'),
            'captcha' => $f3->get('gkt_register_captcha'),
            'lang' => $f3->get('gkt_register_language'),
        ));

        $data = $f3->get('POST');
        $valid = $validator->run($data);

        if ($valid === FALSE) {
            Flash::instance()
                ->addMessage($f3->get('gkt_register_please_fix_the_errors'), 'danger');
            $f3->set('POST', $data);
            $f3->set('ERRORS', $validator->get_errors_array());
            $this->display($f3);
            return;
        }

        $mail = NULL;
        try {
            $this->db->begin();
            // Save user
            $user = new \App\Models\User();
            $user->copyFrom($valid, function($val) {
                return array_intersect_key($val, array_flip(array('user', 'lang')));
            });
            $user->joined = date('Y-m-d H:i:s');
            $user->ip = $f3->get('IP');
            $user->godzina = rand(0, 23); // hu?! Why rand?
            $user->hashPassword($valid['haslo2']);
            $user->genNewSecid();
            $user->save();

            // Generate default avatar as identicon
            $avatar = new \App\Models\Pictures();
            $avatar->genIdenticon($user->userid, $user->user);

            // Save email challenge
            $mail = new \App\Models\ActiveEmails();
            $mail->userid = $user->userid;
            $mail->email = $valid['email'];
            $mail->genVerificationCode($user->userid);
            $mail->save();

            $this->db->commit();
        } catch (\PDOException $e) {
            // Something wrong
            $f3->copy('gkt_error_fail_to_save_data', 'SESSION.ERROR.text');
            $f3->set('SESSION.ERROR.code', 500);
            \App\LogError::exception($e);
            $f3->reroute('@error');
        }

        // Send email
        if (!$mail->sendEmailChallenge($valid['user'])) {
            // Something wrong
            $f3->copy('gkt_error_fail_to_send_email', 'SESSION.ERROR.text');
            $f3->set('SESSION.ERROR.code', 500);
            \App\LogError::message('Failed to send email to: '.$valid['email']);
            $f3->reroute('@error');
        }

        // Log to file
        \App\LogInfo::message('Account created for user: '.$valid['user']);
        Flash::instance()
            ->addMessage($f3->get('gkt_register_account_successfully_created').
                '<br /><br />'.
                $f3->get('gkt_register_an_email_to_confirm_your_email_has_been_sent').
                '<br /><br />'.
                '[<a href="'.$f3->alias('login').'">'.$f3->get('gkt_login').'</a>]'
                , 'success');
        Flash::instance()
            ->addMessage($f3->get('gkt_register_without_email_no_password_recovery'),
                'warning');
        // TODO generate statpic
        $f3->reroute('@home');
    }

}
