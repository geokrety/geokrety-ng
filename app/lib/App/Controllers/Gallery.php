<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;
use \App\Helpers\LogType;
use App\Models\Geokrety;

/**
 * Class Gallery
 * @package App\Controllers
 */
class Gallery extends Controller {

    /**
     * Display Move create page.
     *
     * @param \Base $f3
     */
    public function display($f3) {
        # Pictures
        $pictures = new \App\Models\Pictures();

        if (true) {
            $selectedPictures = $pictures->findGalleryAllPaginate($f3->get('GET.page'), 100);
        } else {
            // TODO filter by uploader
            // TODO filter by owned geokrety
        }
        $f3->set('selectedPictures', $selectedPictures);

        $f3->copy('gkt_gallery_title', 'title');
        $f3->set('content', 'gallery.htm');
        echo \Template::instance()->render('layout.htm');
    }

}
