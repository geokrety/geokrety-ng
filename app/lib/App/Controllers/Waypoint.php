<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

/**
 * Class Waypoint
 * @package App\Controllers
 */
class Waypoint extends Controller {

    /**
     * Check if a waypoint exists in database. Used on Create Move page.
     *
     * @param \Base $f3
     */
    function checkWpt($f3) {
        $wptm = new \App\Models\Waypoint();
        $wpt = $wptm->lookupWaypoint($f3->get('GET.wpt'))[0];
        $isJson = 'application/json' == $f3->get('SERVER.CONTENT_TYPE');

        // Waypoint not found
        if ($wpt === FALSE) {
            if ($isJson) {
                header('Content-Type: application/json');
                echo json_encode(array('lat' => '', 'lon' => ''));
            } else {
                echo \Template::instance()->render('waypoint_not_found.htm');
            }
            return;
        }

        // Waypoint found
        $f3->set('wpt', $wpt);
        if ($isJson) {
            header('Content-Type: application/json');
            echo json_encode(array('lat' => $wpt->lat, 'lon' => $wpt->lon));
        } else {
            echo \Template::instance()->render('waypoint_found.htm');
        }
    }

    /**
     * Check if lat lon coordinates are valid.
     *
     * @param \Base $f3
     */
    function checkCoords($f3) {
        $isJson = 'application/json' == $f3->get('SERVER.CONTENT_TYPE');
        $coords = \App\CoordParse::coords_parse($f3->get('GET.latlon'));

        if (!empty($coords['error'])) {
            if ($isJson) {
                header('Content-Type: application/json');
                $f3->error(404, json_encode($coords));
            } else {
                $f3->status(400);
                echo $coords['error'];
            }
            return;
        }

        if ($isJson) {
            header('Content-Type: application/json');
            echo json_encode($coords);
        } else {
            echo "Valid coords";
        }
    }

    /**
     * Lookup waypoint by name.
     *
     * @param \Base $f3
     */
    function cacheNameSearch($f3) {
        if (empty($f3->get('GET.phrase')) || strlen($f3->get('GET.phrase')) <= 3) {
            return;
        }

        $waypointy = new \App\Models\Waypoint();
        $wpts = $waypointy->getByName($f3->get('GET.phrase'));
        if ($waypointy->dry()) {
            return;
        }

        $result = array();
        foreach ($wpts as $wpt) {
            $result[] = array(
                'lat' => $wpt['lat'],
                'lon' => $wpt['lon'],
                'waypoint' => $wpt['waypoint'],
                'country' => $wpt['country'],
                'name' => $wpt['name'],
                'link' => $wpt['link'],
                'owner' => $wpt['owner'],
                'typ' => $wpt['typ'],
                'kraj' => $wpt['kraj'],
                'status' => $wpt['status'],
                'timestamp' => $wpt['timestamp']
            );
        }

        echo json_encode($result);
    }

}
