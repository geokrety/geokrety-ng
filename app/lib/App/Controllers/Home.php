<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Models\Counters;
use \App\Models\Geokrety;
use \App\Models\Pictures;
use \App\Models\Logs;
use \App\Models\News;
use \App\Models\User;

/**
 * Class Home
 * @package App\Controllers
 */
class Home extends Controller {

    /**
     * Display Home page.
     *
     * @param \Base $f3
     */
    function display($f3) {
        $f3->set('title', 'Home');
        $f3->set('content', 'home.htm');

        $gk_stats_1 = array(
            60826,
            30600,
            35673,
        );
        $gk_stats_2 = array(
            88393100,
            229.73,
            2207.54,
            0.59086,
        );
        $f3->set('gk_stats_1', $gk_stats_1);
        $f3->set('gk_stats_2', $gk_stats_2);

        $news = new News();
        $f3->set('latest_news', $news->latest(2));

        $logs = new Logs();
        $f3->set('latest_logs', $logs->latest(7));

        $pictures = new Pictures();
        $f3->set('latest_pictures', $pictures->latest(10));

        $geokrety = new Geokrety();
        $f3->set('latest_registered', $geokrety->latest(7));
        // TODO should define hasseen

        $user = new User();
        $f3->set('online_users', $user->getOnlineUsers());

        $counters = new Counters();
        $f3->set('visit_stats', $counters->getCounterAndIncrement('index'));

        echo \Template::instance()->render('layout.htm');
    }

}
