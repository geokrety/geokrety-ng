<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;

/**
 * Class Controller
 * @package App\Controllers
 */
abstract class Controller {

    /**
     * @var \Base $f3
     */
    protected $f3;

    /**
     * @var \DB\SQL $db
     */
    protected $db;

    /**
     * Actions to be executed before route
     */
    public function beforeroute() {
        $this->restoreSession();
    }

    /**
     * Actions to be executed after route
     */
    public function afterroute() {
        //echo '- After routing';
    }

    /**
     * Controller constructor.
     */
    function __construct() {
        $f3 = \Base::instance();
        $this->f3 = $f3;

        $dbConfig = $f3->get('db');
        $this->db = new \DB\SQL(
            $dbConfig['dsn'],
            $dbConfig['username'],
            $dbConfig['password'],
            array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
        );
        $f3->clear('db');

        $this->f3->set('DB', $this->db);

        new \DB\SQL\Session($this->db, 'gk-sessions', FALSE, function() {
            // Session hijacking handler
            $f3 = \Base::instance();
            Flash::instance()
                ->addMessage($this->f3->get('gkt_session_hijacking_detected'), 'warning');
            $f3->reroute('@home');
        }, 'CSRF');
    }

    /**
     * Restore authenticated user from Cookie.
     */
    private function restoreSession() {
        if ($this->f3->exists('COOKIE.userid')) {
            $this->f3->copy('COOKIE.userid', 'SESSION.userid');
            $this->f3->copy('COOKIE.user', 'SESSION.user');
            $this->f3->set('SESSION.isLoggedIn', TRUE);
        }
    }

    /**
     * Check if a user is logged in.
     *
     * @return bool
     */
    static public function isLoggedIn() {
        $f3 = \Base::instance();

        return ($f3->exists('SESSION.isLoggedIn') &&
            $f3->get('SESSION.isLoggedIn') == TRUE);
    }

    /**
     * Restrict page access to authenticated users.
     */
    public function authenticatedUsersOnly() {
        if (!self::isLoggedIn()) {
            Flash::instance()->addMessage($this->f3->get('gkt_page_not_for_anonymous',
                $this->f3->alias('login')), 'warning');
            $this->f3->reroute('@home');
        }
    }

}
