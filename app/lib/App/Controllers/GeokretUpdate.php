<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;

/**
 * Class GeokretUpdate
 * @package App\Controllers
 */
class GeokretUpdate extends GeokretCreate {

    /**
     * Common actions on rendering GeoKret update page.
     *
     * @param \Base $f3
     */
    public function display($f3) {
        // Authenticated users only
        $this->authenticatedUsersOnly();

        // Copy data to form
        $gk = $this->loadGeokretOrFail($f3);
        $gk->copyTo('POST');

        $f3->copy('gkt_gk_register_edit_geokret_title', 'title');
        $this->_display($f3);
    }

    /**
     * Load and return a GeoKret, if not found, reroute to error page.
     *
     * @param $f3
     * @return \App\Models\Geokrety
     */
    protected function loadGeokretOrFail($f3) {
        if (!$f3->exists('PARAMS.gkid')) {
            Flash::instance()
                ->addMessage($this->f3->get('gkt_gk_register_edit_id_missing'), 'danger');
            $f3->reroute('@error');
        }

        // Load GeoKret
        $gk = new \App\Models\Geokrety();
        $gk->getById($f3->get('PARAMS.gkid'));
        if ($gk->dry()) {
            // wrong gkid?
            Flash::instance()
                ->addMessage($this->f3->get('gkt_gk_register_edit_id_wrong'), 'danger');
            $f3->reroute('@error');
        }

        // Is Owner?
        if (!$gk->isOwner()) {
            Flash::instance()
                ->addMessage($this->f3->get('gkt_gk_register_edit_reserved_to_owner'),
                    'danger');
            $f3->reroute('@error');
        }

        return $gk;
    }

    /**
     * Update an existing GeoKret.
     *
     * @param \Base $f3
     */
    public function save($f3) {
        // Authenticated users only
        $this->authenticatedUsersOnly();

        $validator = new \Validate();
        $valid = $this->validateForm($validator);

        if ($valid === FALSE) {
            Flash::instance()
                ->addMessage($f3->get('gkt_please_fix_the_errors'), 'danger');
            $f3->set('POST', $data);
            $f3->set('ERRORS', $validator->get_errors_array());
            $this->display($f3);
            return;
        }

        try {
            $this->db->begin();
            // Update GK
            $gk = $this->loadGeokretOrFail($f3);
            $gk->nazwa = $valid['nazwa'];
            $gk->opis = $valid['opis'];
            $gk->typ = $valid['typ'];
            $gk->data =
                date('Y-m-d H:i:s'); // Is it necesasry to update the date? TODO Check with old site
            $gk->timestamp_oc = date('Y-m-d H:i:s'); // Idem
            $gk->save();
            $this->db->commit();
        } catch (\PDOException $e) {
            // Something wrong
            $f3->copy('gkt_error_fail_to_save_data', 'SESSION.ERROR.text');
            $f3->set('SESSION.ERROR.code', 500);
            \App\LogError::exception($e);
            $f3->reroute('@error');
        }

        Flash::instance()
            ->addMessage($f3->get('gkt_gk_register_geokret_successfully_updated'),
                'success');
        $f3->reroute('@geokret(@gkid='.$gk->id.')');
    }

}
