<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

/**
 * Class User
 * @package App\Controllers
 */
class User extends Controller {

    /**
     * Display User page.
     *
     * @param \Base $f3
     */
    public function display($f3) {
        // Authenticated users only
        $this->authenticatedUsersOnly();

        $userid = $f3->get('SESSION.userid');
        if ($f3->exists('PARAMS.userid')) {
            $userid = $f3->get('PARAMS.userid');
        }

        $user = new \App\Models\User();
        $user->countRel('geokrety_owned');
        $user->total_distance_owned =
            'SELECT sum(`gk-geokrety__droga`.`droga`) '.
            'FROM `gk-geokrety` AS `gk-geokrety__droga` '.
            'WHERE `gk-geokrety__droga`.`owner` = `gk-users`.`userid` '.
            'GROUP BY `gk-geokrety__droga`.`owner`';
        $user->countRel('geokrety_moved');
        $user->total_distance_moved =
            'SELECT SUM(`gk-ruchy__sum`.`droga`) '.
            'FROM `gk-ruchy` AS `gk-ruchy__sum` '.
            'WHERE `gk-ruchy__sum`.`user` = `gk-users`.`userid` '.
            'GROUP BY `gk-ruchy__sum`.`user`';

        $user->getById($userid);

        if ($user->dry()) {
            $f3->set('SESSION.ERROR.code', 500);
            $f3->copy('gkt_user_no_such_user', 'SESSION.ERROR.text');
            $f3->reroute('@error');
        }

        $f3->set('user', $user);
        $f3->set('isMyself', $f3->get('SESSION.isLoggedIn') && $f3->get('SESSION.userid') == $user->userid);

        # Pictures
        $pictures = new \App\Models\Pictures();
        $f3->set('pictures', $pictures->findGalleryByUserId($userid));

        $f3->copy('gkt_user_my_page', 'title');
        $f3->set('content', 'user.htm');
        echo \Template::instance()->render('layout.htm');
    }

}
