<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;
use \App\Helpers\LogType;
use App\Models\Geokrety;

/**
 * Class MoveCreate
 * @package App\Controllers
 */
class MoveCreate extends Controller {

    /**
     * @var array $latlon
     */
    protected $latlon = NULL;
    /**
     * @var Geokrety $gk
     */
    protected $gk = NULL;

    /**
     * Common actions on rendering Move create page.
     *
     * @param \Base $f3
     */
    protected function _display($f3) {
        // GK types
        // TODO LogType 'Archive' should only be displayed when logged and has nr parameter+archive
        // TODO LogType 'Grabbed/Dropped' should *not* be available for Human GK Types
        $f3->set('gk_types', LogType::getTypesMoveOrder());

        # LeafLet
        $f3->push('javascripts', 'ui/js/leaflet/leaflet.js');
        $f3->push('css', 'ui/js/leaflet/leaflet.css');

        # Easy Autocomplete
        $f3->push('javascripts',
            'https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js');
        $f3->push('css',
            'https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css');

        $f3->copy('CSRF', 'SESSION.csrf');
        $f3->set('content', 'geokret_move.htm');
        echo \Template::instance()->render('layout.htm');
    }

    /**
     * Display Move create page.
     *
     * @param \Base $f3
     */
    public function display($f3) {
        // does user has home coordinates
        $user = new \App\Models\User();
        $user->getById($f3->get('SESSION.userid'));
        if ($user->hasCoord()) {
            $f3->set('USER.hascoord', TRUE);
            $f3->set('USER.lat', $user->lat);
            $f3->set('USER.lon', $user->lon);
        }

        // Get nr from get parameters
        if ($f3->exists('PARAMS.nr') && !$f3->exists('POST.nr')) {
            $f3->copy('PARAMS.nr', 'POST.nr');
        }

        // Set Date/Time default value
        if (!$f3->exists('POST.date')) {
            $f3->set('POST.date', date('Y-m-d'));
        }
        if (!$f3->exists('POST.time')) {
            $f3->set('POST.time', '12:00');
        }

        $f3->copy('gkt_gk_register_register_a_new_geokret_title', 'title');
        $this->_display($f3);
    }

    /**
     * Validate Move posted data.
     *
     * @param \Validate $validator An existent validator object
     * @return array|bool Validated and filtered form fields or FALSE
     */
    protected function validateForm($validator) {

        // Check Log type
        $validator->add_validator('is_valid_logtype',
            function($field, $input, $param = NULL) {
                return array_key_exists($input[$field], LogType::TYPES);
            });

        // Check NR
        $validator->add_validator('is_valid_nr', function($field, $input, $param = NULL) {
            $this->gk = new \App\Models\Geokrety();
            $this->gk->getByNr($input[$field]);
            return $this->gk->valid();
        });

        // Check LAT LON
        $validator->add_validator('is_valid_latlon',
            function($field, $input, $param = NULL) {
                $latlon = \App\CoordParse::coords_parse($input[$field]);
                if ($latlon[0] !== FALSE) {
                    $this->latlon = $latlon;
                    return TRUE;
                }
                return FALSE;
            });

        // Check if username is the same as connected user
        $validator->add_validator('username_match_connected',
            function($field, $input, $param = NULL) {
                return strtoupper($input[$field]) ==
                    strtoupper($this->f3->get('SESSION.user'));
            });

        // Check captcha
        $validator->add_validator('match_captcha',
            function($field, $input, $param = NULL) {
                return $param == strtoupper($input[$field]);
            });

        // Check if geokrety was born
        $validator->add_validator('was_geokret_born',
            function($field, $input, $param = NULL) {
                if ($this->gk == NULL || $this->gk->dry()) {
                    return TRUE; # Don't report error if GK is not found
                }
                // TODO what happend if time is not filled?
                return new \DateTime($input[$field].'T'.$input['time']) >=
                    new \DateTime($this->gk->data);
            });

        // Check If date is not in future
        $validator->add_validator('date_isnt_in_future',
            function($field, $input, $param = NULL) {
                return new \DateTime($input[$field].'T'.$input['time']) <=
                    new \DateTime();
            });

        // Check If time is valid
        $validator->add_validator('time_is_valid',
            function($field, $input, $param = NULL) {
                return preg_match('/[0-1][0-9]|[2][0-3]:[0-5][0-9]/', $input[$field]);
            });

        // CSRF check
        $validator->add_validator('csrf', function($field, $input, $param = NULL) {
            if ($input[$field] != $this->f3->get('SESSION.csrf')) {
                Flash::instance()
                    ->addMessage($this->f3->get('gkt_validate_csrf_fail'), 'danger');
                return FALSE;
            }
            return TRUE;
        });

        $validation_rules = array(
            'logtype' => 'required|is_valid_logtype',
            'nr' => 'required|exact_len,6|is_valid_nr',
            'username' => 'required',
            'date' => 'required|date|date_isnt_in_future|was_geokret_born',
            'time' => 'required|time_is_valid',
            'comment' => 'max_len,5120',
            'csrf' => 'required|csrf'
            // TODO this should not be required for authenticated users, or only with api?
        );
        if ($this->isLoggedIn()) {
            $validation_rules['username'] .= '|username_match_connected';
        } else {
            $validation_rules['captcha'] =
                'required|match_captcha,'.$this->f3->get('SESSION.register_captcha_code');
        }

        // Some logtype require coord, some not. we should ignore lat lon for such logtype
        $tmp_logtype = $this->f3->get('POST');
        $tmp_logtype = $tmp_logtype['logtype'];
        if (in_array($tmp_logtype, LogType::LOG_WITH_COORDS)) {
            $validation_rules['wpt'] = 'max_len,8';
            $validation_rules['latlon'] = 'required|is_valid_latlon';
        }

        // Add validation rules
        $validator->validation_rules($validation_rules);


        $validator->filter_rules(array(
            'username' => 'trim|sanitize_string',
            'comment' => 'trim|basic_tags',
        ));

        $validator->set_error_messages(array(
            'is_valid_logtype' => $this->f3->get('gkt_validate_logtype'),
            'is_valid_nr' => $this->f3->get('gkt_validate_nr'),
            'date_isnt_in_future' => $this->f3->get('gkt_validate_in_future'),
            'time_is_valid' => $this->f3->get('gkt_validate_time'),
            'was_geokret_born' => $this->f3->get('gkt_validate_before_creation'),
            'username_match_connected' => $this->f3->get('gkt_validate_user_match_connected'),
        ));


        $validator_field_names = array(
            'logtype' => $this->f3->get('gkt_move_action'),
            'nr' => $this->f3->get('gkt_move_tracking_code'),
            'wpt' => $this->f3->get('gkt_move_waypoint'),
            #'name_of_cache' => $this->f3->get('gkt_'),
            'latlon' => $this->f3->get('gkt_move_coordinates'),
            'captcha' => $this->f3->get('gkt_move_enter_code'),
            'username' => $this->f3->get('gkt_move_name'),
            'date' => $this->f3->get('gkt_move_date'),
            'time' => $this->f3->get('gkt_move_time'),
            'comment' => $this->f3->get('gkt_move_comment'),
        );
        if ($this->isLoggedIn()) {
            $validator_field_names['username'] = $this->f3->get('gkt_move_username');
        }
        $validator->set_field_names($validator_field_names);

        // TODO, Some logtype require coord, some not. we should ignore lat lon for such logtype

        return $validator->run($this->f3->get('POST'));
    }

    /**
     * Verify if a log is already in database.
     * Check by GKID, Date, Time, Comment
     *
     * @param array $data Form fields
     * @return bool TRUE if log identical log was already posted
     */
    protected function checkIdenticalLog($data) {
        $log = new \App\Models\Logs();
        $log->getByIdDateTimeComment($this->gk->id, $data['date'], $data['time'],
            $data['comment']);
        if ($log->dry()) {
            // No record found, nice!
            return FALSE;
        }
        Flash::instance()
            ->addMessage($this->f3->get('gkt_move_identical_log_has_been_submited'),
                'danger');
        return TRUE;
    }

    /**
     * Verify if a log is already in database.
     * Check by GKID, Date, Time
     *
     * @param array $data Form fields
     * @return bool TRUE if log identical log was already posted
     */
    protected function checkIdenticalTimeLog($data) {
        $log = new \App\Models\Logs();
        $log->getByIdDateTime($this->gk->id, $data['date'], $data['time']);
        if ($log->dry()) {
            // No record found, nice!
            return FALSE;
        }
        Flash::instance()
            ->addMessage($this->f3->get('gkt_move_there_is_an_entry_with_this_log'),
                'danger');
        return TRUE;
    }

    /**
     * Check if connected user can archive GeoKret
     *
     * @param array $data Form fields
     * @return bool TRUE
     */
    protected function checkArchiveOwn($data) {
        if ($data['logtype'] != LogType::LOGTYPE_ARCHIVED || $this->gk->isOwner()) {
            return FALSE;
        }
        Flash::instance()
            ->addMessage($this->f3->get('gkt_move_your_cannot_archive_not_your_own_geokret'),
                'danger');
        return TRUE;
    }

    /**
     * Create a new Move.
     *
     * @param \Base $f3
     */
    public function save($f3) {
        $validator = new \Validate();
        $valid = $this->validateForm($validator);

        if ($valid === FALSE) {
            Flash::instance()
                ->addMessage($f3->get('gkt_please_fix_the_errors'), 'danger');
            $f3->set('POST', $f3->get('POST'));
            $f3->set('ERRORS', $validator->get_errors_array());
            $this->display($f3);
            return;
        }

        if ($this->checkIdenticalLog($valid) || $this->checkIdenticalTimeLog($valid) ||
            $this->checkArchiveOwn($valid)
        ) {
            $f3->set('POST', $f3->get('POST'));
            $this->display($f3);
            return;
        }


        try {
            $this->db->begin();
            // Create move
            $move = new \App\Models\Logs();
            $move->id = $this->gk->id;
            $move->data = $valid['date'].' '.$valid['time'];
            $move->koment = $valid['comment'];
            $move->logtype = $valid['logtype'];

            // Some logtypes need waypoint/coordinates, some not ;)
            if (in_array($move->logtype, LogType::LOG_WITH_COORDS)) {
                $move->lat = $this->latlon[0];
                $move->lon = $this->latlon[1];
                $move->waypoint = $valid['wpt'];
            }

            // When not logged set username
            if (!$this->isLoggedIn()) {
                $move->username = $valid['username'];
            } else {
                $move->user = $f3->get('SESSION.userid');
            }

            // Set app/ver if defined, else use default values
            if (isset($valid['app']) && !empty($valid['app'])) {
                $move->app = $valid['app'];
                if (isset($valid['ver']) && !empty($valid['ver'])) {
                    $move->app_ver = $valid['ver'];
                }
            } else {
                $move->app = 'www';
                $move->app_ver = '2.0a';
            }

            // Save the move
            $move->save();
            $this->db->commit();
        } catch (\PDOException $e) {
            // Something wrong
            $f3->copy('gkt_error_fail_to_save_data', 'SESSION.ERROR.text');
            $f3->set('SESSION.ERROR.code', 500);
            \App\LogError::exception($e);
            $f3->reroute('@error');
        }

        // Log to file
        \App\LogInfo::message('New Log for gk:'.$this->gk->id);
        Flash::instance()
            ->addMessage($f3->get('gkt_move_successfully_created'),
                'success');
        $f3->reroute('@geokret(@gkid='.$this->gk->id.')');
    }

}
