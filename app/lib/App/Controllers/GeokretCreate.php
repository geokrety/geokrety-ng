<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

use \App\Helpers\Flash;
use App\Helpers\StatPic;

/**
 * Class GeokretCreate
 * @package App\Controllers
 */
class GeokretCreate extends Controller {

    /**
     * Common actions on rendering GeoKret create page.
     *
     * @param \Base $f3
     */
    protected function _display($f3) {
        // GK types
        $f3->set('gk_types', \App\Helpers\GeokretType::getTypes());

        $f3->copy('CSRF', 'SESSION.csrf');
        $f3->set('content', 'register_geokret.htm');
        echo \Template::instance()->render('layout.htm');
    }

    /**
     * Display GeoKret create page.
     *
     * @param \Base $f3
     */
    public function display($f3) {
        // Authenticated users only
        $this->authenticatedUsersOnly();

        // does user has home coordinates
        $user = new \App\Models\User();
        $user->getById($f3->get('SESSION.userid'));
        if ($user->hasCoord()) {
            $f3->set('user_has_set_home_coordinates', TRUE);
        }

        $f3->copy('gkt_gk_register_register_a_new_geokret_title', 'title');
        $this->_display($f3);
    }

    /**
     * Validate GeoKret create form.
     *
     * @param \Validate $validator An existent validator object
     * @return array|bool Validated and filtered form fields or FALSE
     */
    protected function validateForm(&$validator) {
        // Check GK type
        $validator->add_validator('is_valid_gktype',
            function($field, $input, $param = NULL) {
                return array_key_exists($input[$field], \App\Helpers\GeokretType::TYPES);
            });

        // CSRF check
        $validator->add_validator('csrf', function($field, $input, $param = NULL) {
            if ($input[$field] != $this->f3->get('SESSION.csrf')) {
                Flash::instance()
                    ->addMessage($this->f3->get('gkt_validate_csrf_fail'), 'danger');
                return FALSE;
            }
            return TRUE;
        });

        $validator->validation_rules(array(
            'nazwa' => 'required', // not sure about characters range to accept
            'typ' => 'required|is_valid_gktype',
            'opis' => 'max_len,5120',
            'csrf' => 'csrf',
        ));

        $validator->filter_rules(array(
            'nazwa' => 'trim|sanitize_string',
            'opis' => 'trim|basic_tags',
        ));

        $validator->set_error_messages(array(
            'is_valid_gktype' => $this->f3->get('gkt_validate_gktype'),
        ));

        $validator->set_field_names(array(
            'nazwa' => $this->f3->get('gkt_gk_register_geokret_name'),
            'typ' => $this->f3->get('gkt_gk_register_geokret_type'),
            'opis' => $this->f3->get('gkt_gk_register_geokret_comment'),
        ));

        return $validator->run($this->f3->get('POST'));
    }

    /**
     * Create a new GeoKret.
     *
     * @param \Base $f3
     */
    public function save($f3) {
        // Authenticated users only
        $this->authenticatedUsersOnly();

        $validator = new \Validate();
        $valid = $this->validateForm($validator);

        if ($valid === FALSE) {
            Flash::instance()
                ->addMessage($f3->get('gkt_please_fix_the_errors'), 'danger');
            $f3->set('POST', $f3->get('POST'));
            $f3->set('ERRORS', $validator->get_errors_array());
            $this->display($f3);
            return;
        }

        $gk = NULL;
        try {
            $this->db->begin();
            // Create GK
            $gk = new \App\Models\Geokrety();
            $gk->nazwa = $valid['nazwa'];
            $gk->opis = $valid['opis'];
            $gk->owner = $f3->get('SESSION.userid');
            $gk->typ = $valid['typ'];
            $gk->data = date('Y-m-d H:i:s');
            $gk->timestamp_oc = date('Y-m-d H:i:s');
            $gk->generateTrackingCode();
            $gk->save();

            // add eventual log to my home
            if (isset($valid['logAtHome'])) {
                // does user has home coordinates?
                $user = new \App\Models\User();
                $user->getById($f3->get('SESSION.userid'));

                if ($user->hasCoord()) {
                    $log = new \App\Models\Logs();
                    $log->id = $gk->id;
                    $log->lat = $user->lat;
                    $log->lon = $user->lon;
                    $log->country = $user->country;
                    $log->data = date('Y-m-d H:i:s');
                    $log->user = $user;
                    $log->koment = $f3->get('gkt_born_here');
                    $log->logtype = \App\Helpers\LogType::LOGTYPE_DIPPED;
                    $log->data_dodania = date('Y-m-d H:i:s');
                    $log->app = 'www';
                    $log->save();
                }
            } else {
                StatPic::updateStatPic();
            }
            $this->db->commit();
        } catch (\PDOException $e) {
            // Something wrong
            $f3->copy('gkt_error_fail_to_save_data', 'SESSION.ERROR.text');
            $f3->set('SESSION.ERROR.code', 500);
            \App\LogError::exception($e);
            $f3->reroute('@error');
        }
        $statpic = New \App\Helpers\StatPic();
        $statpic->generate($f3->get('SESSION.userid'));

        // Log to file
        \App\LogInfo::message('New GeoKret created:'.$gk->id);
        Flash::instance()
            ->addMessage($f3->get('gkt_gk_register_geokret_successfully_created'),
                'success');
        $f3->reroute('@geokret(@gkid='.$gk->id.')');
    }

}
