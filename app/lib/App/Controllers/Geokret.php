<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Controllers;

/**
 * Class Geokret
 * @package App\Controllers
 */
class Geokret extends Controller {

    /**
     * Display a GeoKret page
     *
     * @param \Base $f3
     */
    public function display($f3) {
        $geokret = new \App\Models\Geokrety();
        $geokret->getById($f3->get('PARAMS.gkid'));
        if (!$geokret) {
            $f3->error(404);
        }

        $gkid = $geokret->id;
        $userid = $f3->get('SESSION.userid');
        $isOwner = $geokret->isOwner($userid);

        $f3->set('geokret', $geokret);
        $f3->set('isOwner', $isOwner);

        $log = new \App\Models\Logs();
        # Flags
        $f3->set('countryFlags', $log->countryFlags($gkid));

        # User has logged
        $hasUserLogged = $log->hasUserLogged($userid, $gkid);
        $f3->set('hasUserLogged', $hasUserLogged);

        # Moves
        $selectedLog = $log->selectLogsByGkIdPaginate($gkid, $f3->get('GET.page'), 25);
        $f3->set('selectedLogs', $selectedLog);

        # Comments
        $comments = new \App\Models\Comments();
        $f3->set('logComments', $comments->getByGkId($gkid));

        # Rating
        $rating = new \App\Models\GeokretRating();
        $f3->set('rating', $rating->rate($gkid));
        $hasUserRated = $rating->hasUserRated($userid, $gkid);
        if (!$hasUserRated && !$isOwner && $hasUserLogged) {
            $f3->set('ratingDisabled', 'false');
            $f3->set('ratingUserMessage', $f3->get('gkt_rating_can_rate'));
        } elseif ($hasUserRated) {
            $f3->set('ratingDisabled', 'false');
            $f3->set('ratingUserMessage', $f3->get('gkt_rating_you_have_already_rated'));
        } elseif ($isOwner) {
            $f3->set('ratingDisabled', 'true');
            $f3->set('ratingUserMessage', $f3->get('gkt_rating_you_cant_rate_own'));
        } else {
            $f3->set('ratingDisabled', 'true');
            $f3->set('ratingUserMessage', $f3->get('gkt_rating_you_cant_rate'));
        }

        # Pictures
        $pictures = new \App\Models\Pictures();
        $f3->set('pictures', $pictures->findGalleryByGkId($gkid));
        $f3->set('logPictures', $pictures->findLogGalleryByGkId($gkid));
        $f3->set('picturesCount', $pictures->countPicturesForGkId($gkid));

        # Watchers
        $watch = new \App\Models\Watch();
        $f3->set('watchersCount', $watch->countWatchers($gkid));
        $f3->set('isWatching', $watch->isWatching($gkid, $userid));


        # jRating
        $f3->push('javascripts', 'ui/js/jRating/jquery/jRating.jquery.min.js');
        $f3->push('css', 'ui/js/jRating/jquery/jRating.jquery.css?ver=3.0');

        # LeafLet
        $f3->push('javascripts', 'ui/js/leaflet/leaflet.js');
        $f3->push('css', 'ui/js/leaflet/leaflet.css');

        $f3->set('title', $geokret->name);
        $f3->set('content', 'geokret.htm');
        echo \Template::instance()->render('layout.htm');
    }

    /**
     * Display GeoKret move as GeoJson.
     *
     * @param \Base $f3
     * @internal param int $limit
     */
    public function geojsonMoves($f3) {
        $geokret = new \App\Models\Geokrety();
        $geokret->getById($f3->get('PARAMS.gkid'));
        if ($geokret->dry()) {
            $f3->error(404);
        }
        $gkid = $geokret->id;

        # Logs
        $limit = max(50, $f3->get('GET.limit')?:50);
        $log = new \App\Models\Logs();
        $selectedLog = $log->selectLogsByGkIdMap($gkid, $f3->get('GET.page'), $limit);
        $f3->set('selectedLogs', $selectedLog);
        $f3->set('totalLogs', count($selectedLog['subset']));

        echo \Template::instance()->render('geokret_moves.json', 'application/json');
    }

    /**
     * Check if a Tracking code is valid. Used on ruchy page.
     *
     * @param \Base $f3
     */
    public function checkNr($f3) {
        $geokret = new \App\Models\Geokrety();
        $geokret->getByNr($f3->get('GET.nr'));
        if ($geokret->dry()) {
            $f3->status(404);
            echo \Template::instance()->render('geokret_not_found.htm');
            return;
        }
        echo "gktype:-".$geokret->id.'-'.$f3->get('GET.nr').'<br>';
        $f3->set('geokret', $geokret);

        $move = new \App\Models\Logs();
        $move->getById($geokret->ost_pozycja_id);
        if (!$move->dry()) {
            $f3->set('last_log', $move);
        }

        echo \Template::instance()->render('geokret_found.htm');
    }

}
