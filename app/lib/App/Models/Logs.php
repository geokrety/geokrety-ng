<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

use App\Helpers\StatPic;

/**
 * Class Logs
 * @package App\Models
 * @property int ruch_id log id
 * @property Geokrety id geokret (id)
 * @property double lat Log latitude
 * @property double lon Log longitude
 * @property int alt Log Altitude
 * @property string country Log Country
 * @property int droga Distance jump
 * @property string waypoint Log waypoint
 * @property string data Move log date
 * @property string data_dodania Move create date?
 * @property int user User id
 * @property string koment Log comment
 * @property int zdjecia photos count
 * @property int komentarze log comment count
 * @property string logtype Log type id
 * @property string username Username (if anonymous)
 * @property double timestamp Move create date?
 * @property string app Application user for logging
 * @property string app_ver Application Version user for logging
 */
class Logs extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-ruchy',
        $primary = 'ruch_id';

    protected $fieldConf = [
        'ruch_id' => [
            'type' => 'INT',
            'nullable' => false
        ],
        'id' => [
            'type' => 'INT',
            'nullable' => false,
            'belongs-to-one' => array('\App\Models\Geokrety', 'id')
        ],
        'lat' => [
            'type' => 'DOUBLE',
            'nullable' => true
        ],
        'lon' => [
            'type' => 'DOUBLE',
            'nullable' => true
        ],
        'alt' => [
            'type' => 'INT',
            'default' => '-32768',
            'nullable' => false
        ],
        'country' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'droga' => [
            'type' => 'INT',
            'nullable' => false
        ],
        'waypoint' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'data' => [
            'type' => 'DATETIME',
            'nullable' => true
        ],
        'data_dodania' => [
            'type' => 'DATETIME',
            'nullable' => true
        ],
        'user' => [
            'type' => 'INT',
            'nullable' => true,
            'belongs-to-one' => array('\App\Models\User', 'userid')
        ],
        'koment' => [
            'type' => 'VARCHAR128',
            'nullable' => true
        ],
        'zdjecia' => [
            'type' => 'INT1',
            'nullable' => false
        ],
        'komentarze' => [
            'type' => 'INT',
            'nullable' => false
        ],
        'logtype' => [
            'type' => 'VARCHAR128',
            'nullable' => true
        ],
        'username' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'timestamp' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => false
        ],
        'app' => [
            'type' => 'VARCHAR128',
            'default' => 'www',
            'nullable' => false
        ],
        'app_ver' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'move_comments' => [
            'has-many' => array('\App\Models\Comments', 'ruch_id')
        ]
    ];

    /**
     * Logs constructor.
     */
    public function __construct() {
        parent::__construct();

        $this->beforeinsert(function($self, $pkeys) {
            $self->setDateCreate();
            $self->zdjecia = 0; // photos count
            $self->komentarze = 0; // log comment count
            $self->setCountry();
            $self->setElevation();
            $self->setDistance();
            $self->fixValuesCase();
        });

        $this->afterinsert(function($self) {
            $self->updateVisitedCacheCount();
            $self->updateLastPositionId();
            $self->updateLastLogId();
            StatPic::updateStatPic(); // Generate for (eventually) connected user
            StatPic::updateStatPic($self->id->owner->user); // GeoKret owner
        });

        $this->beforeupdate(function($self, $pkeys) {
            $self->setCountry();
            $self->setElevation();
            $self->setDistance();
            $self->fixValuesCase();
        });

        $this->afterupdate(function($self) {
            $self->updateVisitedCacheCount();
            $self->updateTotalDistance();
            $self->updateLastPositionId();
            $self->updateLastLogId();
            $self->updateMissingStatus();
            StatPic::updateStatPic(); // Generate for (eventually) connected user
            StatPic::updateStatPic($self->id->owner->user); // GeoKret owner
        });

        $this->aftererase(function($self) {
            $self->updateVisitedCacheCount();
            $self->updateTotalDistance();
            $self->updateLastPositionId();
            $self->updateLastLogId();
            $self->updateMissingStatus();
            StatPic::updateStatPic(); // Generate for (eventually) connected user
            StatPic::updateStatPic($self->id->owner->user); // GeoKret owner
        });
    }

    /**
     * Load $limit last created Moves.
     *
     * @param int $limit Max number of moves to load.
     * @return \DB\CortexCollection
     */
    public function latest($limit = 7) {
        return $this->find(
            array('logtype <> \'2\''),
            array(
                'order' => 'ruch_id DESC',
                'limit' => $limit
            )
        );
    }

    /**
     * Load moves by id.
     *
     * @param int $ruchid Move id.
     * @return Logs
     */
    public function getById($ruchid) {
        $this->load(array('ruch_id=?', $ruchid));
        return $this;
    }

    /**
     * Get Waypoint information from moves.
     *
     * lat , lon , name, typ, country, link, alt, country
     *
     * @param string $waypoint Waypoint name.
     * @return Logs
     */
    public function getCoordByWaypoint($waypoint) {
        return $this->findone(
            ['waypoint=?', $waypoint],
            ['limit' => 1, 'order' => 'data_dodania DESC']
        );
    }

    /**
     * Check if a user has logged a GeoKret.
     * Return False if it has not logged the GeoKret, or the last move.
     *
     * @param int $userid User id.
     * @param int $gkid GeoKret id.
     * @return FALSE|static Move or False.
     */
    public function hasUserLogged($userid, $gkid) {
        return $this->findone(array('`id`=? AND user=? AND `logtype`<>\'2\'', $gkid,
            $userid));
    }

    /**
     * Load moves by GeoKret id as paginated.
     *
     * @param int $gkid GeoKret id.
     * @param int $page Page number.
     * @param int $per Number of entries per page.
     * @return array Moves records.
     */
    public function selectLogsByGkIdPaginate($gkid, $page = 0, $per = 25) {
        $page = \Pagination::findCurrentPage();
        return $this->paginate($page - 1, $per, // page, count
            array('id = ? AND logtype != \'6\'', $gkid),
            array('order' => 'data DESC')
        );
    }

    /**
     * Load moves by GeoKret id as paginated, usable for displaying on a map.
     * Map only show moves of type DROPPED, SEEN, DIPPED.
     *
     * @param int $gkid GeoKret id.
     * @param int $page Page number.
     * @param int $per Number of entries per page.
     * @return array Moves records.
     */
    public function selectLogsByGkIdMap($gkid, $page = 0, $per = 25) {
        $page = \Pagination::findCurrentPage();
        return $this->paginate($page - 1, $per, // page, count
            array('id = ? AND logtype IN (\'0\', \'3\', \'5\')', $gkid),
            array('order' => 'data DESC')
        );
    }

    /**
     * Retrieve GeoKret travelled country flags.
     *
     * @param int $gkid GeoKret id.
     * @return array|FALSE|int Array of country and count place.
     */
    public function countryFlags($gkid) {
        // TODO drop view from query
        return $this->db->exec(
            'SELECT country, count(country) AS count
       FROM (
          SELECT country, IF(country = @prev, @seq, @seq := @seq + 1) AS seq,
           IF(country = @prev, country, @prev := country) AS grp_country
         FROM `gk-ruchy`, (SELECT @seq := 0, @prev := \'\' COLLATE utf8_polish_ci) AS init
         WHERE id=?
         AND country <> \'\'
         ORDER BY data ASC, data_dodania
       ) x
       GROUP BY seq, country', $gkid);
    }

    /**
     * Load GeoKret by id, raise an error if GeoKret does not exists.
     *
     * @return Geokrety Geokrety object.
     */
    protected function getGkOrError() {
        $gk = new \App\Models\Geokrety();
        $gk->getbyId($this->id->id);
        if ($this->dry()) {
            $f3 = \Base::instance();
            $f3->copy('gkt_error_fail_to_save_data', 'SESSION.ERROR.text');
            $f3->set('SESSION.ERROR.code', 500);
            \App\LogError::message('getGkOrError(): Failed to load GK:'.
                $this->id->id);
            $f3->reroute('@error');
        }
        return $gk;
    }

    /**
     * Update GK visited cache count number.
     */
    protected function updateVisitedCacheCount() {
        $gk = $this->getGkOrError();
        $gk->updateVisitedCacheCount();
    }

    /**
     * Update GK total distance.
     */
    protected function updateTotalDistance() {
        $gk = $this->getGkOrError();
        $gk->updateTotalDistance();
    }

    /**
     * Update last position id.
     */
    protected function updateLastPositionId() {
        $gk = $this->getGkOrError();
        $gk->updateLastPositionId();
    }

    /**
     * Update last log id.
     */
    protected function updateLastLogId() {
        $gk = $this->getGkOrError();
        $gk->updateLastLogId();
        $gk->save();
    }

    /**
     * Update missing status.
     */
    protected function updateMissingStatus() {
        $gk = $this->getGkOrError();
        $gk->updateMissingStatus();
    }

    /**
     * Fix case for some fields.
     */
    protected function fixValuesCase() {
        $this->country = strtolower($this->country);
        $this->waypoint = strtoupper($this->waypoint);
    }

    /**
     * Define country based on name, or log latitude/longitude.
     *
     * @param string $country Country name.
     */
    public function setCountry($country = NULL) {
        if (!is_null($country)) {
            $this->country = $country;
            return;
        }
        if (empty($this->lat) || empty($this->lon)) {
            return;
        }
        $this->country = \App\Helpers\Geocoder::getCountry($this->lat, $this->lon);
    }

    /**
     * Define elevation based on value, or log latitude/longitude.
     *
     * @param int|null $elevation Elevation in meters.
     */
    public function setElevation($elevation = NULL) {
        if (!is_null($elevation)) {
            $this->alt = $elevation;
            return;
        }
        if (empty($this->lat) || empty($this->lon)) {
            $this->alt = -7000;
            return;
        }
        $this->alt = \App\Helpers\Geocoder::getElevation($this->lat, $this->lon);
    }

    /**
     * Define jump distance in kilometers, or compute it from last move and
     * latitude/longitude.
     *
     * @param int|null $distance Distance en kilometers.
     */
    public function setDistance($distance = NULL) {
        if (!is_null($distance)) {
            $this->droga = $distance;
            return;
        }
        if (empty($this->lat) || empty($this->lon)) {
            return;
        }
        // Load previous move
        $lastLog = new \App\Models\Logs();
        $lastLog->getLastMoveId($this->id->id);
        if ($lastLog->dry()) {
            $this->droga = 0;
            return;
        }
        $this->droga =
            \App\Helpers\Geocoder::getDistance($lastLog->lat, $lastLog->lon, $this->lat,
                $this->lon);
    }

    /**
     * Define log creation date based on value, or set it to now.
     * @param string|null $date
     */
    public function setDateCreate($date = NULL) {
        if (!is_null($date)) {
            // TODO check date format
            $this->data_dodania = $date;
            return;
        }
        $this->data_dodania = date('Y-m-d H:i:s');
    }

    /**
     * Return the last move id. A comment isn't considered as a move.
     * If no move found, return 0.
     *
     * @param int $gkid GeoKret id.
     * @return int Last move id.
     */
    public function getLastMoveId($gkid) {
        $this->load(
            array('id=? AND logtype IN (\'0\',\'1\',\'3\',\'4\',\'5\')', $gkid),
            array('order' => 'data DESC', 'limit' => 1)
        );

        if ($this->dry()) {
            return 0;
        }
        return $this->ruch_id;
    }

    /**
     * Return the last log id.
     * If no move found, return 0.
     *
     * @param int $gkid GeoKret id.
     * @return int Last move id.
     */
    public function getLastLogId($gkid) {
        $this->load(
            array('id=?', $gkid),
            array('order' => 'data DESC', 'limit' => 1)
        );

        if ($this->dry()) {
            return 0;
        }
        return $this->ruch_id;
    }

    /**
     * Load move log by coordinates.
     *
     * @param double $lat Latitude.
     * @param double $lon Longitude.
     * @return Logs
     */
    public function getByLatLon($lat, $lon) {
        $this->load(array('lat=? AND lon=?', $lat, $lon));
        return $this;
    }

    /**
     * Load move log by date and time.
     *
     * @param int $id GeoKret id.
     * @param string $date Move date.
     * @param string $time Move time.
     * @return Logs
     */
    public function getByIdDateTime($id, $date, $time) {
        $this->load(array('id=? AND data=?', $id, $date.' '.$time));
        return $this;
    }

    /**
     * Load move log by date, time and comment.
     *
     * @param int $id GeoKret id.
     * @param string $date Move date.
     * @param string $time Move time.
     * @param string $comment Move comment.
     * @return Logs
     */
    public function getByIdDateTimeComment($id, $date, $time, $comment) {
        $this->load(array('id=? AND data=? AND koment=?', $id, $date.' '.$time,
            $comment));
        return $this;
    }

}
