<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

/**
 * Class GeokretRating
 * @property mixed id GeoKret id
 * @property User userid User (id)
 * @property int rate The Gk rate
 * @property string totalVote
 * @property string avgVote
 * @package App\Models
 */
class GeokretRating extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-geokrety-rating',
        $primary = 'id';

    protected $fieldConf = [
        'id' => [
            'type' => 'INT8',
            'nullable' => FALSE,
            'belongs-to-one' => array('\App\Models\Geokrety', 'id')
        ],
        'userid' => [
            'type' => 'INT8',
            'nullable' => FALSE,
            'belongs-to-one' => array('\App\Models\User', 'userid')
        ],
        'rate' => [
            'type' => 'DOUBLE',
            'nullable' => FALSE
        ]
    ];

    /**
     * Retrieve vote information for a GeoKret.
     *
     * @param int $gkid GeoKret id.
     * @return array Array of total votes and average vote.
     */
    public function rate($gkid) {
        $this->totalVote = 'COUNT(rate)';
        $this->avgVote = 'AVG(rate)';
        $this->load(
            array('id=?', $gkid),
            array('group' => 'id')
        );

        if ($this->dry()) {
            return ['totalVote' => 0, 'avgVote' => 0];
        }
        return ['totalVote' => $this->totalVote, 'avgVote' => $this->avgVote];
    }

    /**
     * Check if a user has already voted for a Geokret.
     *
     * @param int $userid User id/
     * @param int|null $gkid GeoKret id.
     * @return bool True if user has already voted.
     */
    public function hasUserRated($userid, $gkid = NULL) {
        $gkid = $gkid !== NULL?$gkid:$this->id->id;
        // TODO check if $gkid/$id is defined.
        $rate = $this->findone(array('id=? AND userid=?', $gkid, $userid));
        return $rate->dry();
    }
}
