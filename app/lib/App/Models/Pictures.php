<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

/**
 * Class Pictures
 * @property int typ Pictures type.
 * @property int id_kreta GeoKret id.
 * @property int id Userid or kretid or logid.
 * @property int user user id who posted the picture.
 * @property string plik picture name.
 * @property string opis Comment.
 * @property string timestamp Pictures created date time.
 * @package App\Models
 */
class Pictures extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-obrazki',
        $primary = 'obrazekid';

    protected $fieldConf = [
        'typ' => [
            'type' => 'INT1',
            'nullable' => true
        ],
        'obrazekid' => [
            'type' => 'INT',
            'nullable' => false
        ],
        'id' => [
            'type' => 'INT',
            'nullable' => false,
            'belongs-to-one' => array('\App\Models\User', 'userid')
        ],
        'id_kreta' => [
            'type' => 'INT',
            'nullable' => false,
            'belongs-to-one' => array('\App\Models\Geokrety', 'id')
        ],
        'user' => [
            'type' => 'INT',
            'nullable' => false,
            'belongs-to-one' => array('\App\Models\User', 'userid')
        ],
        'plik' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'opis' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'timestamp' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => false
        ]
    ];

    /**
     * @var array IMG_TYPE Valid picture types.
     */
    const IMG_TYPE = array(
        'GALLERY',
        'COMMENT',
        'AVATAR'
    );

    /**
     * @var int GALLERY
     */
    const GALLERY = 0;
    /**
     * @var int COMMENT
     */
    const COMMENT = 1;
    /**
     * @var int AVATAR
     */
    const AVATAR = 2;

    /**
     * Load $limit last uploaded pictures.
     *
     * @param int $limit Max number of pictures to load.
     * @return \DB\CortexCollection
     */
    public function latest($limit = 5) {
        return $this->find(
            array(), // where
            array(
                'order' => 'timestamp DESC',
                'limit' => $limit
            )
        );
    }

    /**
     * Select pictures gallery for an User.
     *
     * @param int $userid User id.
     * @return \DB\CortexCollection
     */
    public function findGalleryByUserId($userid) {
        return $this->find(
            array('user=? AND typ='.self::AVATAR, $userid),
            array('order' => 'id DESC')
        );
    }

    /**
     * Select pictures gallery for a GeoKret.
     *
     * @param int $gkid GeoKret id.
     * @return \DB\CortexCollection
     */
    public function findGalleryByGkId($gkid) {
        return $this->find(
            array('id_kreta=? AND typ='.self::GALLERY, $gkid),
            array('order' => 'id DESC')
        );
    }

    /**
     * Select pictures gallery for a GeoKret from logs.
     *
     * @param int $gkid GeoKret id.
     * @return \DB\CortexCollection
     */
    public function findLogGalleryByGkId($gkid) {
        return $this->find(
            array('id_kreta=? AND typ='.self::COMMENT, $gkid),
            array('order' => 'timestamp ASC')
        );
    }

    /**
     * Count number of pictures of a GeoKret.
     *
     * @param int $gkid GeoKret id.
     * @return int Number of pictures.
     */
    public function countPicturesForGkId($gkid) {
        return $this->count(array('id_kreta=?', $gkid));
    }

    /**
     * Select all pictures and paginate.
     *
     * @param int $page The page to display.
     * @param int $per How many pictures per pages.
     * @return array Pictures records.
     */
    public function findGalleryAllPaginate($page = 0, $per = 100) {
        $page = \Pagination::findCurrentPage();
        return $this->paginate($page - 1, $per, // page, count
            NULL,
            array('order' => 'timestamp DESC')
        );
    }

    /**
     * Select pictures by sender id and paginate.
     *
     * @param int $senderId The user id to find uploaded pictures.
     * @param int $page The page to display.
     * @param int $per How many pictures per pages.
     * @return array Pictures records.
     */
    public function findGalleryBySenderIdPaginate($senderId, $page = 0, $per = 100) {
        $page = \Pagination::findCurrentPage();
        return $this->paginate($page - 1, $per, // page, count
            array('user=?', $senderId),
            array('order' => 'timestamp DESC')
        );
    }

    /**
     * Select all pictures from all user's GeoKrety and paginate.
     *
     * @param int $ownerId The user id to find
     * @param int $page The page to display.
     * @param int $per How many pictures per pages.
     * @return array Pictures records.
     */
    public function findGalleryByOwnerIdPaginate($ownerId, $page = 0, $per = 100) {
        $page = \Pagination::findCurrentPage();
        return $this->paginate($page - 1, $per, // page, count
            array('id=?', $ownerId),
            array('order' => 'timestamp DESC')
        );
    }

    /**
     * Generate an identicon picture for a user, and set it as avatar.
     *
     * @param int $userid User id.
     * @param string $username Username.
     */
    public function genIdenticon($userid, $username) {
        $f3 = \Base::instance();
        $imgname = \Web::instance()->slug($username);
        $plik = $imgname.'.png';
        $path = $f3->get('ROOT').$f3->get('geokrety.geokrety_pictures').$plik;

        $img = new \Image();
        $identicon = $img->identicon($username);
        $f3->write($path, $identicon->dump('png', 9));

        // Also generate small picture (even if identicon are already small...
        $path_small = $f3->get('ROOT').$f3->get('geokrety.geokrety_pictures_small').$plik;
        $f3->write($path_small, $identicon->dump('png', 9));

        $this->typ = self::AVATAR;
        $this->id = $userid;
        $this->id_kreta = 0;
        $this->user = $userid;
        $this->plik = $plik;
        $this->opis = $f3->get('gkt_my_avatar', $username);
        $this->save();
    }

}
