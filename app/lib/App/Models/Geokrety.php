<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

use App\Helpers\StatPic;
use \App\Helpers\GeokretType;

/**
 * Class Geokrety
 * @package App\Models
 * @property string gk_count number of loaded GeoKret
 * @property string total_distance.
 * @property int id GeoKret id
 * @property string nazwa GeoKret name.
 * @property string nr Tracking code.
 * @property string opis Description.
 * @property User owner Owner.
 * @property int typ GeoKret type.
 * @property string data GeoKret create date.
 * @property string timestamp_oc Updated date time (OC?)
 * @property string timestamp Updated date time
 * @property int ost_log_id Last move id
 * @property string name
 */
class Geokrety extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-geokrety',
        $primary = 'id';

    protected $fieldConf = [
        'id' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'nr' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'nazwa' => [
            'type' => 'VARCHAR128',
            'nullable' => TRUE
        ],
        'opis' => [
            'type' => 'TEXT',
            'nullable' => TRUE
        ],
        'owner' => [
            'type' => 'INT',
            'nullable' => TRUE,
            'belongs-to-one' => array('\App\Models\User', 'userid')
        ],
        'data' => [
            'type' => 'DATETIME',
            'nullable' => TRUE
        ],
        'droga' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'skrzynki' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'zdjecia' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'ost_pozycja_id' => [
            'type' => 'INT',
            'nullable' => FALSE,
            'belongs-to-one' => array('\App\Models\Logs', 'id')
        ],
        'ost_log_id' => [
            'type' => 'INT',
            'nullable' => FALSE,
            'belongs-to-one' => array('\App\Models\Logs', 'id')
        ],
        'missing' => [
            'type' => 'INT1',
            'nullable' => FALSE
        ],
        'typ' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'avatarid' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'timestamp_oc' => [
            'type' => 'DATETIME',
            'nullable' => FALSE
        ],
        'timestamp' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => FALSE
        ],
        'geokret_moves' => [
            'has-many' => array('\App\Models\Logs', 'id')
        ],
        'geokret_moves_comments' => [
            'has-many' => array('\App\Models\Comments', 'kret_id')
        ],
        'geokret_pictures' => [
            'has-many' => array('\App\Models\Pictures', 'id_kreta')
        ],
        'users_watching' => [
            'has-many' => array('\App\Models\User', 'userid')
        ]
    ];

    /**
     * @var int TC_GEN_MAX_TRIES Max tries for generating valid tracking code.
     */
    const TC_GEN_MAX_TRIES = 100;

    /**
     * @var \Base $f3 FatFreeFramework object.
     */
    protected $f3;

    /**
     * Geokrety constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->f3 = \Base::instance();

        // Create new virtual fields
        $this->onload(function($self) {
            $self->virtual('name', $self->name()); // TODO where this name() come from??
            $self->virtual('gkid', self::id2gkid($self->id));
            $self->virtual('type', GeokretType::name($self->typ));
        });

        $this->afterinsert(function($self, $pkeys) {
            StatPic::updateStatpic();
        });

        $this->aftererase(function($self) {
            StatPic::updateStatpic();
        });
    }

    /**
     * Load GeoKret record by GeoKret id.
     *
     * @param int $gkid GeoKret id.
     * @return Geokrety
     */
    public function getById($gkid) {
        $this->load(array('id = ?', $gkid));
        return $this;
    }

    /**
     * Load GeoKret record by GeoKret tracking code.
     *
     * @param string $nr Tracking code.
     * @return Geokrety
     */
    public function getByNr($nr) {
        $this->load(array('nr = ?', $nr));
        return $this;
    }

    /**
     * Load GeoKret moved by a user.
     *
     * @param int $userid User id.
     * @return \DB\CortexCollection
     */
    public function getMoveStatsByOwnerId($userid) {
        return $this->find(array('owner = ? AND typ != 2', $userid));
    }

    /**
     * Load $limit last created GeoKret.
     *
     * @param int $limit Max number of GeoKret to load.
     * @return \DB\CortexCollection
     */
    public function latest($limit = 7) {
        return $this->find(
            array('owner > 0'), // where
            array('order' => 'timestamp_oc DESC', 'limit' => $limit)
        );
    }

    /**
     * Get cleaned GeoKret name. Name can be cut to max len, if size > 0.
     *
     * @param int $size Max name size.
     * @param string|null $name Arbitrary name to be cleaned.
     * @return string Cleaned string.
     */
    protected function name($size = 0, $name = NULL) {
        if ($name === NULL) {
            $name = $this->nazwa;
        }
        return self::nameCleanup($name, $size);
    }

    /**
     * Clean and emojify a GeoKret name. Name can be cut to max len, if size > 0.
     *
     * @param string $name Arbitrary name to be cleaned.
     * @param int $size Max name size.
     * @return string Cleaned string.
     */
    static public function nameCleanup($name, $size = 0) {
        if ($size == 0 || strlen($name) <= $size) {
            return \UTF::instance()->emojify($name);
        }
        return \UTF::instance()->emojify(substr($name, 0, $size).'&#8230;');
    }

    /**
     * Convert a GeoKret id to textual id. (46464 -> GKB580).
     *
     * @param int $gkid GeoKret id.
     * @return string Textual id.
     */
    static public function id2gkid($gkid) {
        return sprintf('GK%04X', $gkid);
    }

    /**
     * Check if a user id is the owner of a GeoKret id.
     * If $userid is not specified, check for the current connected user.
     * If $userid is not specified and no one is logged, return False.
     *
     * @param int|null $userid A user id.
     * @return bool True id userid/connected user is the owner.
     */
    public function isOwner($userid = NULL) {
        if (!\App\Controllers\Controller::isLoggedIn()) {
            return FALSE;
        }
        if (is_null($userid)) {
            $userid = $this->f3->get('SESSION.userid');
        }
        return $userid == $this->owner->userid;
    }

    /**
     * Update GeoKret counter of visited number of caches.
     * Need to be called after a new registered/deleted move.
     */
    public function updateVisitedCacheCount() {
        $this->db->exec(
            'UPDATE `gk-geokrety` AS gk SET skrzynki =
            (SELECT count(DISTINCT(concat(ru.lat,ru.lon)))
              FROM `gk-ruchy` AS ru
              WHERE ru.id=:gkid AND ru.logtype IN (\'0\', \'3\', \'5\'))
            WHERE gk.id=:gkid',
            array('gkid' => $this->id));
    }

    /**
     * Update GeoKret total distance.
     * Need to be called after a new registered/deleted move.
     */
    public function updateTotalDistance() {
        $this->db->exec(
            'UPDATE `gk-geokrety` AS gk SET droga =
            (SELECT sum(droga)
              FROM `gk-ruchy` AS ru
              WHERE ru.id=:gkid)
            WHERE gk.id=:gkid',
            array('gkid' => $this->id));
    }

    /**
     * Update GeoKret last position id field to latest cache id.
     * Need to be called after a new registered/deleted move.
     */
    public function updateLastPositionId() {
        $logs = new Logs();
        $ruch_id = $logs->getLastMoveId($this->id);
        $this->db->exec(
            'UPDATE `gk-geokrety` AS gk SET ost_pozycja_id=?, timestamp_oc=NOW()
        WHERE gk.id=?',
            array($ruch_id, $this->id));
    }

    /**
     * Update GeoKret last log id field to latest log id.
     * Need to be called after a new registered/deleted move.
     */
    public function updateLastLogId() {
        $logs = new Logs();
        $this->ost_log_id = $logs->getLastLogId($this->id);
    }

    /**
     * Update GeoKret missing status dynamically using last log type.
     * Need to be called after a new registered/deleted move.
     */
    public function updateMissingStatus() {
        $this->db->exec(
            'UPDATE `gk-geokrety` AS gk SET missing =
              (SELECT count(*)
               FROM `gk-ruchy-comments` AS co
               WHERE co.ruch_id = gk.ost_pozycja_id AND co.type=\'1\' LIMIT 1),
             timestamp_oc=NOW()
             WHERE gk.id=:gkid',
            array('gkid' => $this->id));
    }

    /**
     * Generate a Tracking Code for a GeoKret.
     * If generation fails, then it redirect to an error page.
     *
     * @return string Tracking code.
     */
    public function generateTrackingCode() {
        $factory = new \RandomLib\Factory;
        $generator = $factory->getLowStrengthGenerator();
        $f3 = $this->f3;

        $tc_size = $f3->get('geokrety.trackingcode_size');
        $tc_alphabet = $f3->get('geokrety.trackingcode_alphabet');
        $tracking_code = NULL;
        $limit = 0;
        while ($limit++ <= self::TC_GEN_MAX_TRIES || is_null($tracking_code)) {
            // generate
            $tracking_code = $generator->generateString($tc_size, $tc_alphabet);

            // check if TC looks like a GK code
            if (preg_match('#^gk[0-9a-f]{4}$#i', $tracking_code)) {
                $tracking_code = NULL;
                continue;
            }

            // check if TC is already in use
            $res = $this->db->exec('SELECT count(*) FROM `gk-geokrety` WHERE nr = ?',
                $tracking_code);
            // failed to check DB :/
            if ($res === FALSE) {
                $tracking_code = NULL;
                $f3->copy('gkt_error_fail_to_generate_tracking_code',
                    'SESSION.ERROR.text');
                $f3->set('SESSION.ERROR.code', 500);
                \App\LogError::message('Failed to generate Tracking Code - SQL check TC in DB');
                $f3->reroute('@error');
            }
            // check if in use
            if ($res[0][0] != 0) {
                $tracking_code = NULL;
                continue;
            }

            // TC should be ok
            break;
        }

        // has succeed?
        if (is_null($tracking_code)) {
            \App\LogError::message('Trouble while generating Tracking Code - '.$limit.
                ' attempts');
            $f3->copy('gkt_error_fail_to_generate_tracking_code', 'SESSION.ERROR.text');
            $f3->set('SESSION.ERROR.code', 500);
            \App\LogError::message('Failed to generate Tracking Code - Too much attempts ('.
                $limit.' attempts)');
            $f3->reroute('@error');
        }

        // Abnormal attempts while generating?
        if ($limit >= 10) {
            \App\LogError::message('Trouble while generating Tracking Code - '.$limit.
                ' attempts');
        }

        \App\LogInfo::message('New Tracking Code generated - '.$limit.' attempts');
        $this->nr = $tracking_code;
        return $this->nr;
    }

}
