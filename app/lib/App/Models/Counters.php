<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

/**
 * Class Counters
 * @property string avg Average counter value
 * @property mixed licznik Counter value
 * @property mixed od Start date
 * @property string witryna Counter name
 * @package App\Models
 */
class Counters extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-licznik';

    protected $fieldConf = [
        'witryna' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'licznik' => [
            'type' => 'INT8',
            'nullable' => FALSE
        ],
        'od' => [
            'type' => 'DATETIME',
            'nullable' => FALSE
        ]
    ];


    /**
     * Load all records.
     *
     * @return Counters
     */
    public function all() {
        $this->load();
        return $this;
    }

    /**
     * Load or create a counter.
     *
     * @param string $name Counter name.
     * @return Counters
     */
    private function loadOrCreate($name) {
        $this->avg = 'licznik / ((NOW() - od) / 86400)';
        $this->load(array('witryna=?', $name));
        if ($this->dry()) { # no previous record
            $this->initializeCounter($name); # create first entry
        }
        return $this;
    }

    /**
     * Retrieve a counter name, average, start date.
     *
     * @param string $name Counter name.
     * @return array Name, average, start date.
     */
    public function getCounter($name) {
        $this->loadOrCreate($name);
        return $this->formatStats();
    }

    /**
     * Retrieve an incremented counter name, average, start date.
     *
     * @param string $name Counter name.
     * @param int $increment Increment value.
     * @return array Name, average, start date.
     */
    public function getCounterAndIncrement($name, $increment = 1) {
        $this->loadOrCreate($name);
        # Don't count users twice
        $f3 = \Base::instance();
        if (!$f3->exists('SESSION.geokret.counter.'.$name)) {
            $this->licznik += $increment;
            $this->save();
            $this->loadOrCreate($name); // Reload to get accurate value
            $f3->set('SESSION.geokret.counter.'.$name, TRUE);
        }
        return $this->formatStats();
    }

    /**
     * Initialize a counter.
     *
     * @param string $name Counter name.
     * @param int $start_value Starting value.
     */
    private function initializeCounter($name, $start_value = 0) {
        $this->witryna = $name;
        $this->licznik = $start_value;
        $this->touch('od');
        $this->save();
    }

    /**
     * Format stat as an array.
     *
     * @return array Name, average, start date.
     */
    private function formatStats() {
        return array($this->licznik, sprintf('%.1f', $this->avg), $this->od);
    }

}
