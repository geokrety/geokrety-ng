<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;
/**
 * Class Waypoint
 * @package App\Models
 */
class Waypoint extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-waypointy';


    protected $fieldConf = [
        'waypoint' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'lat' => [
            'type' => 'DOUBLE',
            'nullable' => FALSE
        ],
        'lon' => [
            'type' => 'DOUBLE',
            'nullable' => FALSE
        ],
        'alt' => [
            'type' => 'INT',
            'default' => '-32768',
            'nullable' => FALSE
        ],
        'country' => [
            'type' => 'VARCHAR128',
            'nullable' => TRUE
        ],
        'name' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'owner' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'typ' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'kraj' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'link' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'status' => [
            'type' => 'INT1',
            'default' => '1',
            'nullable' => FALSE
        ],
        'timestamp' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => FALSE
        ]
    ];

    /**
     * @var array PREFIXES_OC
     */
    const PREFIXES_OC = array('OC', 'OP', 'OK', 'GE', 'OZ', 'OU', 'ON', 'OL', 'OJ', 'OS',
        'GD', 'GA', 'VI', 'MS', 'TR', 'EX', 'GR', 'RH', 'OX', 'OB', 'OR');
    /**
     * @var array PREFIXES_OTHER
     */
    const PREFIXES_OTHER = array('GC');
    /**
     * @var array PREFIXES_OTHER_1
     */
    const PREFIXES_OTHER_1 = array('N');
    /**
     * @var array PREFIXES_OTHER_3
     */
    const PREFIXES_OTHER_3 = array('WPG');

    /**
     * Load waypoint by name.
     *
     * @param string $name Waypoint name.
     * @return array Waypoints records.
     */
    public function getByName($name) {
        $this->load(['name like ?', '%'.$name.'%'],
            ['limit' => 10]
        );
        return $this->query;
    }

    /**
     * Load Waypoint point waypoint.
     *
     * @param string $waypoint Waypoint.
     * @return array Waypoint record.
     */
    public function getCoordByWaypoint($waypoint) {
        return $this->findone(
            ['waypoint=?', $waypoint],
            ['limit' => 1]
        )->cast();
    }

    /**
     * Load waypoint by waypoint. Return an array of waypoint and formatted link.
     *
     * @param string $waypoint Waypoint.
     * @return array Array of Waypoint and link.
     */
    public function lookupWaypoint($waypoint) {
        $waypoint = strtoupper($waypoint);
        $wp2 = substr($waypoint, 0, 2);
        $wp3 = substr($waypoint, 0, 3);
        if (in_array($wp2, Waypoint::PREFIXES_OC) ||
            in_array($wp3, Waypoint::PREFIXES_OTHER_3)
        ) {
            $wayp = new Waypoint();
            $wpt = $wayp->getCoordByWaypoint($waypoint);
            if (!empty($wpt)) {
                return array($wpt, $wpt['link']);
            }
        }

        $link = '';
        if (in_array($wp2, Waypoint::PREFIXES_OTHER)) {
            if ($wp2 == 'GC') {
                $link =
                    'https://www.geocaching.com/seek/cache_details.aspx?wp='.$waypoint;
            } elseif (substr($waypoint, 0, 1) == 'N') {
                $link = 'http://www.navicache.com/cgi-bin/db/displaycache2.pl?CacheID='.
                    hexdec(substr($waypoint, 1, 10));
            }
            $wayp = new Logs();
            $wpt = $wayp->getCoordByWaypoint($waypoint);
            if (!empty($wpt)) {
                $wpt['link'] = $link;
                return array($wpt, $link);
            }
        }

        return array(FALSE, $link);
    }

}
