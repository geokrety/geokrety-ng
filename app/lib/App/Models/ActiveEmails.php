<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

use \App\Swistak;

/**
 * @property int userid User id.
 * @property string email User email.
 * @property bool|string kod The validation code.
 * @property mixed done Current kod status
 */
class ActiveEmails extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-aktywnemaile';

    protected $fieldConf = [
        'kod' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'userid' => [
            'type' => 'INT8',
            'nullable' => FALSE,
            'belongs-to-one' => array('\App\Models\User', 'userid')
        ],
        'email' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'done' => [
            'type' => 'INT1',
            'nullable' => FALSE
        ],
        'timestamp' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => FALSE
        ]
    ];

    /**
     * Token is not confirmed.
     */
    const KOD_UNCONFIRMED = 0;

    /**
     * Token has been confirmed.
     */
    const KOD_CONFIRMED = 1;

    /**
     * Load Token records by activation code.
     *
     * @param string $kod Activation code.
     * @return \DB\Cortex
     */
    public function getByToken($kod) {
        $this->load(array('kod=?', $kod));
        return $this;
    }

    /**
     * Generate an activation code for a user.
     *
     * @param int $userid Username id.
     * @return string Activation code.
     */
    public function genVerificationCode($userid) {
        $random = Swistak::getRandomString(4, 1, Swistak::$alphabet_azAZ);
        $tmp = time()."$random$userid";

        $this->kod = Swistak::zawin($tmp, 4, 4);
        return $this->kod;
        //6EKlXQVsSnyDbAYQGUfBlLbL5WjnH2TI = 32bytes
    }

    /**
     * Check an activation code validity, and return included tokens.
     *
     * @param string $kod An activation code.
     * @return bool|array FALSE if invalid or array of token.
     */
    static public function checkVerificationCode($kod) {
        if (preg_match('#^([\d]{10})[a-zA-Z]{4}([\d]+)$#', Swistak::rozwin($kod, 4, 4),
            $matches)) {
            return $matches;
        } else {
            return FALSE;
        }
    }

    /**
     * Send an activation code to an user.
     *
     * @param string|null $username An eventual username to display in mail headers.
     * @return \App\Smtp Smtp result object.
     */
    public function sendEmailChallenge($username = NULL) {
        $f3 = \Base::instance();
        $smtp = \App\Smtp::instance()->setAsHtml();
        // TODO check if email is really set.
        $smtp->to($this->email, $username);
        $smtp->subject($f3->get('gkt_register_activate_your_account'));
        $smtp->bodyFromTemplate('mail_confirm_email.htm', array('kod' => $this->kod));
        $res = $smtp->send();
        if (!$res) {
            Flash::instance()
                ->addMessage($f3->get('gkt_register_failed_to_send_email'), 'danger');
        }
        return $res;
    }

}
