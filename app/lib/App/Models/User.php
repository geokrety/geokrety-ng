<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

use Hautelook\Phpass\PasswordHash;

/**
 * Class User
 * @package App\Models
 * @property integer userid User id.
 * @property false|string joined User joined date.
 * @property mixed ip User ip when registering.
 * @property int godzina Hour?
 * @property mixed user Username.
 * @property mixed statpic Statpic template id.
 * @property string haslo2 Password (new format).
 * @property string secid Api token.
 * @property double lat Home latitude.
 * @property double lon Home longitude.
 * @property string country Home country.
 * @property mixed email user mail address
 */
class User extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-users',
        $primary = 'userid';

    protected $fieldConf = [
        'userid' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'user' => [
            'type' => 'VARCHAR128',
            'nullable' => TRUE
        ],
        'haslo' => [
            'type' => 'VARCHAR128',
            'nullable' => TRUE
        ],
        'haslo2' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'email' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'joined' => [
            'type' => 'DATETIME',
            'nullable' => TRUE
        ],
        'wysylacmaile' => [
            'type' => 'VARCHAR128',
            'default' => '1',
            'nullable' => FALSE
        ],
        'ip' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'timestamp' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => FALSE
        ],
        'lang' => [
            'type' => 'VARCHAR128',
            'nullable' => TRUE
        ],
        'lat' => [
            'type' => 'DOUBLE',
            'nullable' => TRUE
        ],
        'lon' => [
            'type' => 'DOUBLE',
            'nullable' => TRUE
        ],
        'promien' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'country' => [
            'type' => 'VARCHAR128',
            'nullable' => TRUE
        ],
        'godzina' => [
            'type' => 'INT4',
            'nullable' => FALSE
        ],
        'statpic' => [
            'type' => 'INT1',
            'default' => '1',
            'nullable' => TRUE
        ],
        'ostatni_mail' => [
            'type' => 'DATETIME',
            'nullable' => TRUE
        ],
        'ostatni_login' => [
            'type' => 'DATETIME',
            'nullable' => FALSE
        ],
        'secid' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'news' => [
            'has-many' => array('\App\Models\News', 'news_id')
        ],
        'move_comments' => [
            'has-many' => array('\App\Models\Comments', 'user_id')
        ],
        'geokrety_rating' => [
            'has-many' => array('\App\Models\GeokretRating', 'id')
        ],
        'geokrety_moved' => [
            'has-many' => array('\App\Models\Logs', 'user')
        ],
        'avatar' => [
            'has-one' => array('\App\Models\Pictures', 'id')
        ],
        'badges' => [
            'has-many' => array('\App\Models\Badges', 'userid')
        ],
        'posted_pictures' => [
            'has-many' => array('\App\Models\Pictures', 'user')
        ],
        'geokrety_watched' => [
            'has-many' => array('\App\Models\Geokrety', 'id')
        ],
        'geokrety_owned' => [
            'has-many' => array('\App\Models\Geokrety', 'owner')
        ],
        'activation_kod' => [
            'has-one' => array('\App\Models\ActiveEmails', 'userid')
        ]
    ];

    /**
     * User constructor.
     */
    public function __construct() {
        parent::__construct();

        $this->afterinsert(function($self, $pkeys) {
            \App\Helpers\StatPic::updateStatpic($self->userid);
        });
    }

    /**
     * Load all users.
     *
     * @return User
     */
    public function all() {
        $this->load();
        return $this;
    }

    /**
     * Load user by id.
     *
     * @param int $id User id.
     * @return User
     */
    public function getById($id) {
        $this->load(array('userid=?', $id));
        return $this;
    }

    /**
     * Load user by name.
     *
     * @param string $name Username.
     * @return User
     */
    public function getByName($name) {
        $this->load(array('user=?', strval($name)));
        return $this;
    }

    /**
     * Load user by email.
     *
     * @param string $email
     * @return User
     */
    public function getByEmail($email) {
        $this->load(array('email=?', strval($email)));
        return $this;
    }

    /**
     * Load user if registration was less than $time.
     *
     * @param string $name Username
     * @param int $time Time limit in seconds.
     * @return User
     */
    public function isRecentlyRegistered($name, $time = 3600) {
        $this->load(array(
            'user=? AND `email`=\'\' AND (NOW()-`joined` < ?) AND `ostatni_login`=0 AND `ip`=?',
            $name, $time, \Base::instance()->get('IP')
        ));
        return $this;
    }

    /**
     * Load online users.
     *
     * @return \DB\CortexCollection
     */
    public function getOnlineUsers() {
        return $this->find(array('ostatni_login > DATE_SUB(NOW(), INTERVAL 5 MINUTE)'));
    }

    /**
     * Update user from POST values.
     *
     * @param int $id User id.
     */
    public function edit($id) {
        $this->load(array('userid=?', $id));
        $this->copyFrom('POST');
        $this->update();
    }

    /**
     * Set last visit for an user.
     */
    public function setLastVisit() {
        $this->touch('ostatni_login');
    }

    /**
     * Delete an user.
     *
     * @param int $id User id.
     */
    public function delete($id) {
        $this->load(array('userid=?', $id));
        $this->erase();
    }

    /**
     * Check if user has defined home coordinates.
     *
     * @return bool True if home coordinates are defined.
     */
    public function hasCoord() {
        return !$this->dry() && !empty($this->lat) && !empty($this->lon) &&
            !is_null($this->lat) && !is_null($this->lon);
    }

    /**
     * Hash user password.
     *
     * @param string $password Password to hash.
     * @return string Hashed password.
     */
    public function hashPassword($password) {
        $f3 = \Base::instance();
        $passwordHasher = new PasswordHash(11, FALSE);
        $this->haslo2 =
            $passwordHasher->HashPassword($password.$f3->get('geokrety.passwd_salt').
                '127');
        return $this->haslo2;
    }

    /**
     * Check password against stored user password.
     *
     * @param string $password Password to check.
     * @return bool True if password match.
     */
    public function check_password($password) {
        $f3 = \Base::instance();
        $passwordHasher = new PasswordHash(11, FALSE);
        return $passwordHasher->CheckPassword($password.$f3->get('geokrety.passwd_salt').
            '127', $this->haslo2);
    }

    /**
     * Generate a new secid token.
     *
     * @param int $size Size of the secid token.
     */
    public function genNewSecid($size = 128) {
        $factory = new \RandomLib\Factory;
        $generator = $factory->getLowStrengthGenerator();
        $this->secid = $generator->generateString($size);
    }

}
