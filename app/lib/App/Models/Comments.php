<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

/**
 * Class Comments
 * @package App\Models
 */
class Comments extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-ruchy-comments',
        $primary = 'comment_id';

    protected $fieldConf = [
        'comment_id' => [
            'type' => 'INT',
            'nullable' => false
        ],
        'ruch_id' => [
            'type' => 'INT',
            'nullable' => false,
            'relationType' => array('\App\Models\Logs', 'ruch_id')
        ],
        'kret_id' => [
            'type' => 'INT',
            'nullable' => false,
            'relationType' => array('\App\Models\Geokrety', 'id')
        ],
        'user_id' => [
            'type' => 'INT',
            'nullable' => false,
            'relationType' => array('\App\Models\User', 'userid')
        ],
        'data_dodania' => [
            'type' => 'DATETIME',
            'nullable' => false
        ],
        'comment' => [
            'type' => 'VARCHAR128',
            'nullable' => false
        ],
        'type' => [
            'type' => 'INT1',
            'nullable' => false
        ],
        'timestamp' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => false
        ]
    ];

    /**
     * Load Comments records by GeoKret id.
     *
     * @param int $gkid GeoKret id.
     * @return Comments
     */
    public function getByGkId($gkid) {
        $this->load(array('kret_id=?', $gkid));
        return $this;
    }

}
