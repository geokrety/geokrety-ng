<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Models;

//SELECT DATE(`date`), `tresc`, `tytul`, `who`, `userid`, `komentarze`, `news_id` FROM `gk-news` ORDER BY `date` DESC LIMIT 2

/**
 * Class News
 * @package App\Models
 */
class News extends \DB\Cortex {

    protected
        $db = 'DB',
        $table = 'gk-news',
        $primary = 'news_id';

    protected $fieldConf = [
        'news_id' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'date' => [
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'nullable' => FALSE
        ],
        'czas_postu' => [
            'type' => 'DATETIME',
            'nullable' => FALSE
        ],
        'tytul' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'tresc' => [
            'type' => 'TEXT',
            'nullable' => TRUE
        ],
        'who' => [
            'type' => 'VARCHAR128',
            'nullable' => FALSE
        ],
        'userid' => [
            'belongs-to-one' => array('\App\Models\User', 'userid')
        ],
        'komentarze' => [
            'type' => 'INT',
            'nullable' => FALSE
        ],
        'ostatni_komentarz' => [
            'type' => 'DATETIME',
            'nullable' => TRUE
        ]
    ];

    /**
     * Load $limit last news.
     *
     * @param int $limit Max number of news to load.
     * @return \DB\CortexCollection
     */
    public function latest($limit = 2) {
        return $this->find(
            array(), // where
            array(
                'order' => 'date DESC',
                'limit' => $limit
            )
        );
    }

}
