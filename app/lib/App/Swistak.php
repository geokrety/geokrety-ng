<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App;

/**
 * Class Swistak
 * @package App
 */
class Swistak {
    /**
     * @var string $key
     */
    // TODO is this a secret value???
    private static $key = 'geokreci_biznes';
    /**
     * @var string $iv32
     */
    // TODO is this a secret value???
    private static $iv32 = '12ca6d6a339c30a5acc9fd785cb76d0a';

    /**
     * @var string $alphabet_full
     */
    public static $alphabet_full = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    /**
     * @var string $alphabet_azAZ
     */
    public static $alphabet_azAZ = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    /**
     * @var string $algorithm
     */
    private static $algorithm = MCRYPT_RIJNDAEL_128;

    /**
     * @param string $data
     * @return string
     */
    public static function safe_b64encode($data) {
        $data = base64_encode($data);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);

        return $data;
    }

    /**
     * @param string $data
     * @return string
     */
    public static function safe_b64decode($data) {
        $data = str_replace(array('-', '_'), array('+', '/'), $data);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }

        return base64_decode($data);
    }

    /**
     * @param int $length
     * @param int $max_repeat_chars max_repeat_chars - ile razy moze sie powtorzyc jakis znak  (np dla wartosci 0 i alfabetu 'ABCD', nigy nie uzyskamy ciagu 'AABC' i nigdy ciag nie bedzie dluzszy od alfabetu)
     * @param string $alphabet
     * @return string
     */
    public static function getRandomString($length, $max_repeat_chars = 2,
        $alphabet = '') {
        if (empty($alphabet)) {
            $alphabet = self::$alphabet_full;
        }
        if ($max_repeat_chars > 0) {
            $alphabet .= str_repeat($alphabet, $max_repeat_chars);
        }

        return substr(str_shuffle($alphabet), 0, $length);
    }

    /**
     * @param string $data
     * @param bool $raw
     * @return string
     */
    private static function haszuj($data, $raw = TRUE) {
        return md5($data, $raw);
    }

    // iv_length - dlugosc wygenerowanego iv (wektora inicjujacego), reszta (32-iv_length) jest stala w zmiennej $iv32. max to 32 bajty.
    // hash_length - dlugosc hasha dolaczona do wiadomosci. max to 16 bajtow bo jest to wartosc 'raw'.
    // *** nalezy pamietac ze jezeli podamy inne wartosci tych parametrow, to przy rozwijaniu musimy rowniez uzyc tych samych wartosci (!)

    /**
     * @param string $data
     * @param int $iv_length
     * @param int $hash_length
     * @return bool|string
     */
    public static function zawin($data, $iv_length = 4, $hash_length = 4) {
        if (!$data) {
            return FALSE;
        }
        $iv_prefix = self::getRandomString($iv_length, 0);
        //$iv_size = mcrypt_get_iv_size(swistak::$algorithm, MCRYPT_MODE_OFB); //	echo "[[[$iv_size]]]";
        // $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $hash = substr(self::haszuj($data), 0, $hash_length);
        $enc = mcrypt_encrypt(self::$algorithm, md5(self::$key), $hash.$data,
            MCRYPT_MODE_OFB, $iv_prefix.substr(self::$iv32, 0, 16 - $iv_length));
        $b64 = self::safe_b64encode($enc);

        return $iv_prefix.$b64;
    }

    /**
     * @param string $data
     * @param int $iv_length
     * @param int $hash_length
     * @return bool|string
     */
    public static function rozwin($data, $iv_length = 4, $hash_length = 4) {
        if (!$data) {
            return FALSE;
        }
        $iv_prefix = substr($data, 0, $iv_length);
        $b64 = substr($data, $iv_length);
        $dec =
            @mcrypt_decrypt(self::$algorithm, md5(self::$key), self::safe_b64decode($b64),
                MCRYPT_MODE_OFB, $iv_prefix.substr(self::$iv32, 0, 16 - $iv_length));
        $hash = substr($dec, 0, $hash_length);
        $cleartext = substr($dec, $hash_length);
        if ($hash != substr(self::haszuj($cleartext), 0, $hash_length)) {
            return FALSE;
        }

        return trim($cleartext);
    }

}
