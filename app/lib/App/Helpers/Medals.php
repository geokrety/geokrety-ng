<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class Medals
 * @package App\Helpers
 */
class Medals extends \App\Helpers\BaseHelper {

    /**
     * @var string IMG_DIR Medals picture path
     */
    const IMG_DIR = '/ui/images/medals/';

    /**
     * @var \Base $f3
     */
    protected $f3;

    /**
     * Medals constructor.
     */
    public function __construct() {
        $this->f3 = \Base::instance();
    }

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();

        $count = self::resolveAttr($attr, 'count', 0);

        return "<?php echo \\App\\Helpers\\Medals::instance()->build($count); ?>";
    }

    /**
     * Get an array of obtained medals.
     * key is text award ; value medal image name
     *
     * @param int $count GeoKret count
     * @return array Obtained Medals
     */
    static protected function awards($count){
        $awards = [];
        if ($count >= 1) { $awards['1'] = 'medal-1-1.png'; }
        if ($count >= 10) { $awards['10'] = 'medal-1-2.png'; }
        if ($count >= 20) { $awards['20'] = 'medal-1-3.png'; }
        if ($count >= 50) { $awards['50'] = 'medal-1-4.png'; }
        if ($count >= 100) { $awards['100'] = 'medal-bialy.png'; }
        if ($count >= 120) { $awards['5! = 120'] = 'medal-120.png'; }
        if ($count >= 200) { $awards['200'] = 'medal-brazowy.png'; }
        if ($count >= 314) { $awards['100* Pi = 100 * 3.14 = 314'] = 'medal-pi.png'; }
        if ($count >= 500) { $awards['500'] = 'medal-srebrny.png'; }
        if ($count >= 512) { $awards['2^9 = 512'] = 'medal-512.png'; }
        if ($count >= 720) { $awards['6! = 1*2*3*4*5*6 = 720'] = 'medal-720.png'; }
        if ($count >= 800) { $awards['800'] = 'medal-zloty.png'; }
        if ($count >= 1000) { $awards['1000'] = 'medal-1000.png'; }
        if ($count >= 1024) { $awards['2^10 = 1024'] = 'medal-1024.png'; }
        if ($count >= 2000) { $awards['2000'] = 'medal-2000.png'; }
        if ($count >= 3000) { $awards['3000'] = 'medal-3000.png'; }
        if ($count >= 5000) { $awards['5000'] = 'medal-5000.png'; }
        if ($count >= 5040) { $awards['7! = 1*2*3*4*5*6*7 = 5040'] = 'medal-5040.png'; }
        if ($count >= 10000) { $awards['10000'] = 'medal-10000.png'; }
        return $awards;
    }

    /**
     * Render HTML for custom tag.
     *
     * @param int $count The GeoKret count.
     * @return string Html tag string.
     */
    public function build($count) {
        $awards = self::awards($count);

        $html = '';
        foreach ($awards as $key => $image) {
            $title = $this->f3->get('gkt_medals_award_for_x_geokrety', $key);
            $html .= '<img src="'.self::IMG_DIR.$image.'" alt="'.$title.'" title="'.$title.'" />';
        }
        return $html;
    }

}
