<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class LogoImage
 * @package App\Helpers
 */
class LogoImage extends \App\Helpers\BaseHelper {

    /**
     * Return image corresponding to date.
     *
     * @param string $month_day The date as 'mmdd' string.
     * @return array Array of image path and translated Title.
     */
    static private function _get_image($month_day) {
        $f3 = \Base::instance();
        if ($month_day == '1206') {
            return array('img' => 'ui/images/logo/logo-mikolajki.png',
                'title' => $f3->get('logo_saint_nicholas'));
        } elseif ($month_day == '1026') {
            return array('img' => 'ui/images/logo/logo-urodziny.png',
                'title' => $f3->get('logo_geokrety_birthday'));
        } elseif ($month_day >= '1224' && $month_day <= '1228') {
            return array('img' => 'ui/images/logo/logo-swieta2.png',
                'title' => $f3->get('logo_merry_christmas'));
        } elseif ($month_day >= '1231' ||
            ($month_day >= '0101' && $month_day <= '0104')
        ) {
            return array('img' => 'ui/images/logo/logo-sylwestra.png',
                'title' => $f3->get('logo_happy_new_year'));
        } elseif ($month_day == '0214') {
            return array('img' => 'ui/images/logo/logo-walentynki.png',
                'title' => $f3->get('logo_valentine_day'));
        } elseif ($month_day == '0401') {
            return array('img' => 'ui/images/logo/geozyrafy.png',
                'title' => $f3->get('logo_april_fool_day'));
        } elseif ($month_day == '1031') {
            return array('img' => 'ui/images/logo/geokrety+pumpkin.png',
                'title' => $f3->get('logo_halloween'));
        } elseif ($month_day == '0327') {
            return array('img' => 'ui/images/logo/logo-wielkanoc2.png',
                'title' => $f3->get('logo_easter'));
        } elseif ($month_day >= '0321' && $month_day <= '0329') {
            return array('img' => 'ui/images/logo/logo-wiosna.png',
                'title' => $f3->get('logo_spring'));
        } elseif ($month_day == '0110') {
            return array('img' => 'ui/images/logo/geokrety+wosp.png',
                'title' => $f3->get('logo_christmas_charity'));
        }
        return array('img' => 'ui/images/logo/logo-puste.png',
            'title' => $f3->get('logo_it_s_today'));
    }

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        return '<?php echo \App\Helpers\LogoImage::instance()->build(date(\'md\')); ?>';
    }

    /**
     * Render HTML for custom tag.
     *
     * @param string $month_day The date as 'mmdd' string.
     * @return string Html tag string.
     */
    public function build($month_day) {
        $image = \App\Helpers\LogoImage::_get_image($month_day);
        $out = '<img src="'.$image['img'].'" border="0" alt="'.$image['title'].'" />';
        return $out;
    }
}
