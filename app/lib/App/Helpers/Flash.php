<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class Flash
 * @package App\Helpers
 */
class Flash extends \App\Helpers\BaseHelper {

    /**
     * @var \Base f3
     */
    protected $f3;
    /**
     * @var array msg Array of messages
     */
    protected $msg;

    /**
     * Flash constructor.
     *
     * @param string $key A named key in the hive for those flash messages.
     */
    public function __construct($key = 'flash') {
        $this->f3 = \Base::instance();
        $this->msg = &$this->f3->ref('SESSION.'.$key.'.msg');
    }

    /**
     * Store a new message.
     * Status are ones from Boostrap.
     *
     * @param string $text The message text.
     * @param string $status The status text.
     */
    public function addMessage($text, $status = 'info') {
        $this->msg[] = array('text' => $text, 'status' => $status);
    }

    /**
     * Get stored messages and clear them from memory.
     *
     * @return array
     */
    public function getMessages() {
        $out = $this->msg;
        $this->clearMessages();
        return $out;
    }

    /**
     * Clear stored messages.
     */
    public function clearMessages() {
        $this->msg = array();
    }

}
