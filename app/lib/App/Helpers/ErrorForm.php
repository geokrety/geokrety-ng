<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class ErrorForm
 * @package App\Helpers
 */
class ErrorForm extends \Prefab {

    /**
     * Display an error as HTML, used in form checks.
     *
     * @param string $error The error message.
     * @param string $class CSS class.
     * @return string HTML formatted error message.
     */
    static public function printError($error, $class = 'error text-danger') {
        return '<span class="'.$class.'">'.$error.'</span>';
    }

    /**
     * Return a single HTML formatted error as string, from single string or array.
     * @param string|array $errors The error messages.
     * @param string $class CSS class
     * @return string HTML formatted error messages.
     */
    static public function printErrors($errors, $class = 'error text-danger') {
        $msgs = '';
        if (is_array($errors)) {
            foreach ($errors as $error) {
                $msgs .= self::printError($error, $class).'<br />';
            }
        } else {
            $msgs .= self::printError($errors, $class).'<br />';
        }
        return $msgs;
    }
}
