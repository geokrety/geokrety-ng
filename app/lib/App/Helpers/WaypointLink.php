<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

// `lat`, `lon`, `name`, `typ`, `kraj`, `link`, `alt`, `country`, `status` => gk-waypointy
// `lat` , `lon` , NULL, NULL, `country`, NULL, `alt`, `country => gk-ruchy


namespace App\Helpers;

/**
 * Class WaypointLink
 *
 * @package App\Helpers
 */
class WaypointLink extends \App\Helpers\BaseHelper {

    /**
     * @var \Base $f3
     */
    protected $f3;

    /**
     * WaypointLink constructor.
     */
    public function __construct() {
        $this->f3 = \Base::instance();
    }

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        if (!isset($node[0])) {
            // Error: Invalid tag. Missing value.
            return '';
        }
        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();

        $lat = self::resolveAttr($attr, 'lat');
        $lon = self::resolveAttr($attr, 'lon');
        $class = self::resolveAttr($attr, 'class', 'bardzomale');
        $waypoint = self::resolveAttr($node, 0, 'Unknown');
        $escape = self::resolveAttr($attr, 'escape', 0);

        return "<?php echo \\App\\Helpers\\WaypointLink::instance()->build($waypoint, $lat, $lon, $class, $escape); ?>";
    }

    /**
     * Render HTML for custom tag.
     *
     * @param string $waypoint The waypoint id.
     * @param double $lat Waypoint latitude.
     * @param double $lon Waypoint Longitude.
     * @param string $class CSS classes.
     * @param bool $escape Add slashes to the output Html string.
     * @return string Html tag string.
     */
    // TODO CHECK THIS MESS!!!
    public function build($waypoint, $lat, $lon, $class, $escape) {
        $wptm = new \App\Models\Waypoint($this->f3->get('DB'));
        $wpt = $wptm->lookupWaypoint($waypoint);
        $link = $wpt[1];
        $wpt = $wpt[0];

        if ($wpt === FALSE && empty($wpt) && empty($lat) && empty($lon)) {
            $result = '<a href="'.$link.'" target="_blank">'.$waypoint.'</a>';
            return $escape?addslashes($result):$result;
        }

        $span = '<span class="'.$class.'" title="Waypoint @ '.$lat.' '.$lon.'">';
        if (!empty($wpt['waypoint'])) {
            $result = $span.'<a href="'.$link.'" target="_blank">'.$waypoint.
                '</a></span>';
            return $escape?addslashes($result):$result;
        } elseif ($wpt['link'] != '') {
            $result =
                $span.'<a href="'.$link.'" target="_blank">'.$waypoint.'</a></span>';
            return $escape?addslashes($result):$result;
        }

        $linka =
            htmlspecialchars("http://www.geocaching.com/seek/nearest.aspx?origin_lat=$lat&origin_long=$lon&dist=1",
                ENT_QUOTES);
        $result =
            $span.$lat.'/'.$lon.'</span> <span class="xs">(<a href="'.$linka.'" title="'.
            $this->f3->get('gkt_search_for_caches_in_this_area_on_geocaching').'">'.
            $this->f3->get('gkt_search_geocaching').'</a>)</span>';

        return $escape?addslashes($result):$result;
    }

}
