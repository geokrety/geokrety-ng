<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class GeokretLink
 * @package App\Helpers
 */
class GeokretLink extends \App\Helpers\BaseHelper {

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();

        $userid = self::resolveAttr($attr, 'userid', 0);
        $gkid = self::resolveAttr($attr, 'gkid', 0);
        $size = self::resolveAttr($attr, 'size', 0);
        $name = self::resolveAttr($node, 0);

        return "<?php echo \\App\\Helpers\\GeokretLink::instance()->build($name, $userid, $gkid, $size); ?>";
    }

    /**
     * Render HTML for custom tag.
     *
     * @param string $name The GeoKret name.
     * @param int $userid The GeoKret owner id.
     * @param int $gkid The GeoKret id.
     * @param int $size The max len of the GeoKret name.
     * @return string Html tag string.
     */
    public function build($name, $userid, $gkid, $size) {
        $f3 = \Base::instance();

        if ($size > 0 && strlen($name) > 0 && strlen($name) > $size ) {
            $name = substr($name, 0, $size).'&#8230;';
        }

        if ($gkid == 0) { // Is this an user ?
            $page = \Base::instance()->alias('user', 'userid='.$userid);
            $name = empty($name)?'['.$f3->get('gkt_link').']':
                \UTF::instance()->emojify($name);
        } else { // Or a GeoKret
            $page = \Base::instance()->alias('geokret', 'gkid='.$gkid);
            $name =
                empty($name)?sprintf('GK%04X', $gkid):\UTF::instance()->emojify($name);
        }

        $title = $f3->get('gkt_go_to_geokret_page');

        return '<a href="'.$page.'" title="'.$title.'">'.$name.'</a>';
    }

}
