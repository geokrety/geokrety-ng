<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class GeokretType
 * @package App\Helpers
 */
class GeokretType extends \App\Helpers\BaseHelper {

    /**
     * @var string IMG_DIR GeoKret type picture path
     */
    const IMG_DIR = '/ui/images/gktypes/';

    /**
     * @var array TYPES GeoKret type names
     */
    const TYPES = array(
        'gkt_gktype_traditional',
        'gkt_gktype_book',
        'gkt_gktype_human',
        'gkt_gktype_coin',
        'gkt_gktype_kretyPost',
    );

    /**
     * @var int GKTYPE_TRADITIONAL
     */
    const GKTYPE_TRADITIONNAL = 0;
    /**
     * @var int GKTYPE_BOOK
     */
    const GKTYPE_BOOK = 1;
    /**
     * @var int GKTYPE_HUMAN
     */
    const GKTYPE_HUMAN = 2;
    /**
     * @var int GKTYPE_COIN
     */
    const GKTYPE_COIN = 3;
    /**
     * @var int GKTYPE_KRETYPOST
     */
    const GKTYPE_KRETYPOST = 4;

    /**
     * @var \Base $f3
     */
    protected $f3;

    /**
     * GeokretType constructor.
     */
    public function __construct() {
        $this->f3 = \Base::instance();
    }

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();

        $type = self::resolveAttr($attr, 'type', 0);
        $width = self::resolveAttr($attr, 'width', 25);
        $height = self::resolveAttr($attr, 'height', 25);
        $class = self::resolveAttr($attr, 'class', 'logicon');
        $alt = self::resolveAttr($attr, 'alt', $node[0]);
        $title = self::resolveAttr($attr, 'title', $node[0]);

        return "<?php echo \\App\\Helpers\\GeokretType::instance()->build($type, $width, $height, $class, $alt, $title); ?>";
    }

    /**
     * Get translated name from type id.
     *
     * @param int $typeId The GeoKret type id.
     * @return string The textual (translated) type.
     */
    static public function name($typeId) {
        $f3 = \Base::instance();
        return $f3->get(self::TYPES[$typeId]);
    }

    /**
     * Get an array of translated Geokret Types.
     *
     * @return array The translated Geokret Types.
     */
    static public function getTypes() {
        return array_map('self::translate', self::TYPES);
    }

    /**
     * Render HTML for custom tag.
     *
     * @param int $type The GeoKret type id.
     * @param int $width The picture width.
     * @param int $height The Picture height.
     * @param string $class The Css class.
     * @param string $alt The picture alternative text.
     * @param string $title The picture title.
     * @return string Html tag string.
     */
    public function build($type, $width, $height, $class, $alt, $title) {
        $typeName = self::name($type);
        $alt = !empty($alt)?$alt:$typeName;
        $title = !empty($title)?$title:$typeName;
        return '<img class="'.$class.'" src="'.self::IMG_DIR.$type.'/icon_25.jpg" alt="'.
            $alt.'" title="'.$title.'" width="'.$width.'" height="'.$height.'" />';
    }

}
