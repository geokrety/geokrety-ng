<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class Pagination
 * @package App\Helpers
 */
class Pagination extends \App\Helpers\BaseHelper {

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {

        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();

        $uri = self::resolveAttr($attr, 'uri');
        $id = self::resolveAttr($attr, 'id', 0);
        $cur = self::resolveAttr($attr, 'cur');
        $class = self::resolveAttr($attr, 'class', 'bold');

        $txt = self::resolveAttr($node, '0', $id);

        return "<?php echo \\App\\Helpers\\Pagination::instance()->build($uri, $id, $cur, $class, $txt); ?>";
    }

    /**
     * Render HTML for custom tag.
     *
     * @param string $uri Custom url.
     * @param int $id Page number.
     * @param int $cur The current page id.
     * @param string $class CSS classes.
     * @param string $txt Tag text.
     * @return string Html tag string.
     */
    public function build($uri, $id, $cur, $class, $txt) {
        if ($cur == $id) {
            $style = $class;
        }
        return '<a href="'.$uri.'?page='.$id.'" class="'.$style.'">'.$txt.'</a>';
    }

}
