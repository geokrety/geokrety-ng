<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class Badge
 * @package App\Helpers
 */
class Badge extends \App\Helpers\BaseHelper {

    /**
     * @var string IMG_DIR Badges picture path
     */
    const IMG_DIR = '/ui/images/badges/';

    /**
     * @var \Base $f3
     */
    protected $f3;

    /**
     * Badges constructor.
     */
    public function __construct() {
        $this->f3 = \Base::instance();
    }

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();

        $image = self::resolveAttr($attr, 'image', 0);
        $title = self::resolveAttr($attr, 'title', '');

        return "<?php echo \\App\\Helpers\\Badge::instance()->build($image, $title); ?>";
    }

    /**
     * Render HTML for custom tag.
     *
     * @param string $image Badge image name
     * @param string $title Badge title
     * @return string Html tag string.
     */
    public function build($image, $title) {
        return '<img src="'.self::IMG_DIR.$image.'" alt="badge" title="'.$title.'" />';
    }

}
