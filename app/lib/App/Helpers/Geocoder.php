<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class Geocoder
 * @package App\Helpers
 */
class Geocoder extends \Prefab {

    /**
     * Get country name from latitude longitude.
     * Try to get it from 'geocoder' api. If unavailable, obtain it from a less detailed
     * source, but locally.
     *
     * @param double $lat The latitude.
     * @param double $lon The longitude.
     * @return string The country name.
     */
    static public function getCountry($lat, $lon) {
        $f3 = \Base::instance();
        $move = new \App\Models\Logs();
        $move->getByLatLon($lat, $lon);
        if (!$move->dry()) {
            return strtolower($move->country);
        }

        $country = file_get_contents($f3->get('geocoder.url').'/api/getCountry?lat='.$lat.
            '&lon='.$lon);
        if ($country === FALSE) {
            \App\LogError::message('Failed to obtain country for lat:'.$lat.' lon:'.$lon);

            // fallback to local library
            return \DaveRoss\OfflineCountryReverseGeocoder\get_country($lon, $lat);
        }
        return strtolower($country);
    }

    /**
     * Get elevation in meters from latitude longitude.
     * Try to get it from 'geocoder' api. If unavailable, return -7000.
     *
     * @param double $lat The latitude.
     * @param double $lon The longitude.
     * @return int The altitude.
     */
    static public function getElevation($lat, $lon) {
        $f3 = \Base::instance();
        $move = new \App\Models\Logs();
        $move->getByLatLon($lat, $lon);
        if (!$move->dry()) {
            return $move->alt;
        }

        try {
            // TODO it may break when remote server return code 500. Adding @ in front let this continue but is it really efficient?
            $alt =
                file_get_contents($f3->get('geocoder.url').'/api/getElevation?lat='.$lat.
                    '&lon='.$lon);
        } catch (Exception $e) {
            $alt = FALSE;
        }
        finally {
            if ($alt === FALSE) {
                \App\LogError::message('Failed to obtain elevation for lat:'.$lat.' lon:'.
                    $lon);
                return -7000;
            }
        }
        return (double)$alt;
    }

    /**
     * Get the distance in meters between two coordinates.
     *
     * @param double $lat1 The latitude from first coordinates.
     * @param double $lon1 The longitude from first coordinates.
     * @param double $lat2 The latitude from second coordinates.
     * @param double $lon2 The longitude from second coordinates.
     * @return int The distance between two coordinates.
     */
    static public function getDistance($lat1, $lon1, $lat2, $lon2) {
        $geotools = new \League\Geotools\Geotools();
        $coordA = new \League\Geotools\Coordinate\Coordinate([$lat1, $lon1]);
        $coordB = new \League\Geotools\Coordinate\Coordinate([$lat2, $lon2]);
        $distance = $geotools->distance()->setFrom($coordA)->setTo($coordB);
        return $distance->in('km')->flat();
    }

}
