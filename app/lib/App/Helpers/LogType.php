<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class LogType
 * @package App\Helpers
 */
class LogType extends \App\Helpers\BaseHelper {

    /**
     * @var string IMG_DIR GeoKret log type image path
     */
    const IMG_DIR = '/ui/images/gktypes/';

    /**
     * @var array TYPES GeoKret log type names
     */
    const TYPES = array(
        'gkt_logtype_dropped',  // 0
        'gkt_logtype_grabbed',  // 1
        'gkt_logtype_comment',  // 2
        'gkt_logtype_seen',     // 3
        'gkt_logtype_archived', // 4
        'gkt_logtype_dipped',   // 5
    );

    /**
     * @var array TYPES_MOVE_ORDER
     */
    const TYPES_MOVE_ORDER = array(
        self::LOGTYPE_DROPPED => 'gkt_move_logtype_dropped',
        self::LOGTYPE_GRABBED => 'gkt_move_logtype_grabbed',
        self::LOGTYPE_SEEN => 'gkt_move_logtype_seen',
        self::LOGTYPE_DIPPED => 'gkt_move_logtype_dipped',
        self::LOGTYPE_COMMENT => 'gkt_move_logtype_comment',
        self::LOGTYPE_ARCHIVED => 'gkt_move_logtype_archived',
    );

    /**
     * @var array LOG_WITH_COORDS
     */
    const LOG_WITH_COORDS = array(
        self::LOGTYPE_DROPPED,
        self::LOGTYPE_SEEN,
        self::LOGTYPE_DIPPED
    );

    /**
     * @var int LOGTYPE_DROPPED
     */
    const LOGTYPE_DROPPED = 0;
    /**
     * @var int LOGTYPE_GRABBED
     */
    const LOGTYPE_GRABBED = 1;
    /**
     * @var int LOGTYPE_COMMENT
     */
    const LOGTYPE_COMMENT = 2;
    /**
     * @var int LOGTYPE_SEEN
     */
    const LOGTYPE_SEEN = 3;
    /**
     * @var int LOGTYPE_ARCHIVED
     */
    const LOGTYPE_ARCHIVED = 4;
    /**
     * @var int LOGTYPE_DIPPED
     */
    const LOGTYPE_DIPPED = 5;

    /**
     * @var \Base $f3
     */
    protected $f3;

    /**
     * LogType constructor.
     */
    public function __construct() {
        $this->f3 = \Base::instance();
    }

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        if (!isset($node[0])) {
            // Error: Invalid tag. Missing value.
            return '';
        }
        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();

        $gktype = self::resolveAttr($attr, 'gktype', 0);
        $logtype = self::resolveAttr($attr, 'logtype', 0);
        $width = self::resolveAttr($attr, 'width', 25);
        $height = self::resolveAttr($attr, 'height', 25);
        $class = self::resolveAttr($attr, 'class', 'logtypeicon');
        $alt = self::resolveAttr($attr, 'alt', $node[0]);
        $title = self::resolveAttr($attr, 'title', $node[0]);

        return "<?php echo \\App\\Helpers\\LogType::instance()->build($gktype, $logtype, $width, $height, $class, $alt, $title); ?>";
    }

    /**
     * Get translated name from type id.
     *
     * @param int $type The Log type id.
     * @return mixed The textual (translated) type.
     */
    public static function name($type) {
        $f3 = \Base::instance();
        return $f3->get(LogType::TYPES[$type]);
    }

    /**
     * Get an array of translated Log Types. Order the usual way.
     *
     * @return array The translated Logs Types.
     */
    static public function getTypesMoveOrder() {
        return array_map('self::translate', self::TYPES_MOVE_ORDER);
    }

    /**
     * Render HTML for custom tag.
     *
     * @param int $gktype GeoKret type id.
     * @param int $logtype Log type id.
     * @param int $width Picture width.
     * @param int $height Picture Height.
     * @param string $class CSS classes.
     * @param string $alt Picture alternative.
     * @param string $title Picture title.
     * @return string Html tag string.
     */
    public function build($gktype, $logtype, $width, $height, $class, $alt, $title) {
        $typeName = self::name($logtype);
        $alt = !empty($alt)?$alt:$typeName;
        $title .= (empty($title)?'':': ').$typeName;
        return '<img class="'.$class.'" src="'.self::IMG_DIR.$gktype.'/'.$logtype.
            '.png" alt="'.$alt.'" title="'.$title.'" width="'.$width.'" height="'.$height.
            '" />';
    }

}
