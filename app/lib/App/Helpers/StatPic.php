<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

use App\Models\Logs;

/**
 * Class StatPic
 * @package App\Helpers
 */
class StatPic {

    /**
     * @var array TEMPLATES Ids of known templates.
     */
    const TEMPLATES = array(
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9
    );

    /**
     * Update users statpict.
     * If no userid given, then we generate statpic for currently authenticated user.
     * If no user connected, just de nothing.
     *
     * @param null|int $userid User Id to generate statpic
     */
    static public function updateStatPic($userid = NULL) {
        $f3 = \Base::instance();
        if (is_null($userid)) {
            $userid = $f3->get('SESSION.userid');
        }
        if (!isset($userid)) {
            return;
        }
        self::generate($userid);
    }

    /**
     * Generate a user statpic as png from a given template id.
     *
     * @param int $userid User id.
     */
    static public function generate($userid) {
        $f3 = \Base::instance();

        $user = new \App\Models\User();
        $user->countRel('geokrety_moved');
        $user->total_distance =
            'SELECT SUM(`gk-ruchy__sum`.`droga`) '.
            'FROM `gk-ruchy` AS `gk-ruchy__sum` '.
            'WHERE `gk-ruchy__sum`.`user` = `gk-users`.`userid` '.
            'GROUP BY `gk-ruchy__sum`.`user`';

        $user->countRel('geokrety_owned');
        $user->total_distance_owned =
            'SELECT sum(`gk-geokrety__droga`.`droga`) '.
            'FROM `gk-geokrety` AS `gk-geokrety__droga` '.
            'WHERE `gk-geokrety__droga`.`owner` = `gk-users`.`userid` '.
            'GROUP BY `gk-geokrety__droga`.`owner`';

        $user->getById($userid);

        if ($user->dry()) {
            \App\Helpers\Flash::instance()
                ->addMessage($f3->get('gkt_statpic_unknown_user'), 'danger');
            return;
        }

        if (!in_array($user->statpic, self::TEMPLATES)) {
            \App\Helpers\Flash::instance()
                ->addMessage($f3->get('gkt_statpic_unrecognized_template_id'), 'danger');
            return;
        }

        try {
            $imgname =
                $f3->get('ROOT').'/ui/images/statpics/templates/'.$user->statpic.'.png';
            $img = @imagecreatefrompng($imgname);

            if (!$img) {
                \App\Helpers\Flash::instance()
                    ->addMessage($f3->get('gkt_statpic_failed_to_load_statpic_template'),
                        'danger');
                return;
            }

            $czarny = imagecolorallocate($img, 0, 0, 0);
            $czerwony = imagecolorallocate($img, 0, 0, 0);
            $font = $f3->get('UI').'fonts/RobotoCondensed-Regular.ttf';
            $font2 = $f3->get('UI').'fonts/RobotoCondensed-Regular.ttf';

            $bigzise = 13;
            $smallsize = 9;

            imagettftext($img, $bigzise, 0, 74, 16, $czarny, $font, $user->user);
            imagettftext($img, $smallsize, 0, 74, 31, $czarny, $font2, 'moved:');
            imagettftext($img, $smallsize, 0, 74, 46, $czerwony, $font2, 'owns:');

            imagettftext($img, $smallsize, 0, 114, 31, $czarny, $font2,
                ($user->count_geokrety_moved?:0).' GK');
            imagettftext($img, $smallsize, 0, 114, 46, $czerwony, $font2,
                ($user->count_geokrety_owned?:0).' GK');

            imagettftext($img, $smallsize, 0, 154, 31, $czarny, $font2,
                ': '.($user->total_distance?:0).'km');
            imagettftext($img, $smallsize, 0, 154, 46, $czerwony, $font2,
                ': '.($user->total_distance_owned?:0).'km');

            imagepng($img, $f3->get('ROOT').'/ui/images/statpics/'.$user->userid.'.png');
            imagedestroy($img);
        } catch (\Exception $e) {
            \App\Helpers\Flash::instance()
                ->addMessage($f3->get('gkt_failed_to_generate_statpic'), 'danger');
        }
    }

}
