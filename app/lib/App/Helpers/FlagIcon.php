<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class FlagIcon
 * @package App\Helpers
 */
class FlagIcon extends \App\Helpers\BaseHelper {

    /**
     * @var string FLAG_DIR Flag picture path
     */
    const FLAG_DIR = '/ui/images/country_codes/';

    /**
     * Render custom HTML Tag to php.
     *
     * @param array $node Tag attributes.
     * @return string Php code.
     */
    static public function render($node) {
        if (!isset($node[0])) {
            // Error: Invalid tag. Missing value.
            return '';
        }
        $country = self::resolveAttr($node, 0);

        $attr = array_key_exists('@attrib', $node) ? $node['@attrib']:array();
        $width = self::resolveAttr($attr, 'width', 16);
        $height = self::resolveAttr($attr, 'height', 11);
        $border = self::resolveAttr($attr, 'border', 0);
        $class = self::resolveAttr($attr, 'class');
        $alt = self::resolveAttr($attr, 'alt', $node[0]);
        $title = self::resolveAttr($attr, 'title', $node[0]);

        return "<?php echo \\App\\Helpers\\FlagIcon::instance()->build($country, $width, $height, $border, $class, $alt, $title); ?>";
    }

    /**
     * Render HTML for custom tag.
     *
     * @param string $country The country.
     * @param integer $width Picture width.
     * @param integer $height Picture Height.
     * @param integer $border Picture border.
     * @param string $class Picture Css class.
     * @param string $alt Picture alternative text.
     * @param string $title Picture title.
     * @return string Html tag string.
     */
    public function build($country, $width, $height, $border, $class, $alt, $title) {
        if (empty($country)) {
            return '';
        }
        return '<img class="'.$class.'" src="'.self::FLAG_DIR.$country.'.png" alt="'.$alt.
            '" title="'.$title.'" width="'.$width.'" height="'.$height.'" border="'.
            $border.'" />';
    }

}
