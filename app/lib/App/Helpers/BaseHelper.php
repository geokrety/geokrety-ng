<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App\Helpers;

/**
 * Class BaseHelper
 * @package App\Helpers
 */
class BaseHelper extends \Prefab {

    /**
     * Substitute F3 variables to php variables from Helpers templates.
     *
     * @param array $attr
     * @param mixed $key
     * @param string $default Default value, if unset or empty.
     * @return string Value usable in string.
     */
    static protected function resolveAttr($attr, $key, $default = '') {

        if (!is_array($attr)) {
            return self::resolveAttr(array($default), 0, $default);

        }
        if (!array_key_exists($key, $attr)) {
            return self::resolveAttr(array($default), 0, $default);
        }

        $value = $attr[$key];
        if (preg_match('/{{(.+?)}}/s', $value)) {
            return \Template::instance()->token($value);
        }

        if (!empty($value) || isset($value)) {
            if (is_string($value)) {
                return '\''.$value.'\'';
            }
            return $value;
        }

        if (is_string($default)) {
            return '\''.$default.'\'';
        }
        if (is_null($default)) {
            return '\'\'';
        }
        return $default;
    }

    /**
     * Get key from hive. Used as a translator for LogType and GeokretType.
     *
     * @param string $name A key in the hive
     * @return string A value from the hive
     */
    static protected function translate($name) {
        $f3 = \Base::instance();
        return $f3->get($name);
    }

}
