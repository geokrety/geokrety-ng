<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App;

/**
 * Class Smtp
 * @package App
 */
class Smtp extends \Prefab {

    /**
     * @var \Base $f3
     */
    protected $f3;
    /**
     * @var \SMTP $smtp
     */
    protected $smtp;
    /**
     * @var string $msg
     */
    protected $msg;

    /**
     * Smtp constructor.
     */
    public function __construct() {
        $this->f3 = \Base::instance();
        $this->reset();
    }

    /**
     * Initialize a new Smtp message.
     *
     * @return Smtp $this Smtp object.
     */
    private function reset() {
        $smtpconf = $this->f3->get('smtp');
        $this->smtp = new \SMTP($smtpconf['host'], $smtpconf['port'], $smtpconf['scheme'],
            $smtpconf['username'], $smtpconf['password']);
        $this->smtp->set('From', '"'.$this->f3->get('geokrety.namefrom').'" <'.
            $this->f3->get('geokrety.mailfrom').'>');
        $this->smtp->set('Errors-to', '"'.$this->f3->get('geokrety.namefrom').'" <'.
            $this->f3->get('geokrety.mailfrom').'>');
        $this->smtp->set('Return-Path', '"'.$this->f3->get('geokrety.namefrom').'" <'.
            $this->f3->get('geokrety.mailfrom').'>');
        $this->setAsText();
        unset($this->msg);
        return $this;
    }

    /**
     * Declare the mail as Html.
     *
     * @return Smtp $this Smtp object.
     */
    public function setAsHtml() {
        $this->smtp->set('MIME-Version', '1.0');
        $this->smtp->set('Content-Type', 'text/html; charset=UTF-8');
        return $this;
    }

    /**
     * Declare the mail as Text.
     *
     * @return Smtp $this Smtp object.
     */
    public function setAsText() {
        $this->smtp->clear('MIME-Version');
        $this->smtp->set('Content-Type', 'text/plain; charset=UTF-8');
        return $this;
    }

    /**
     * Define recipient address.
     *
     * @param string $email Recipient address
     * @param string $username Recipient name
     * @return Smtp $this Smtp object.
     */
    public function to($email, $username = NULL) {
        if (is_null($username)) {
            $this->smtp->set('To', $email);
        } else {
            $this->smtp->set('To', "\"$username\" <$email>");
        }
        return $this;
    }

    /**
     * Define mail subject.
     *
     * @param string $subject mail subject
     * @return Smtp $this Smtp object.
     */
    public function subject($subject) {
        $this->smtp->set('Subject', '[GeoKrety] '.$subject);
        return $this;
    }

    /**
     * Define mail body from a template.
     *
     * @param string $template template name to use
     * @param array $values couple of values to register in hive
     * @return Smtp $this Smtp object.
     */
    public function bodyFromTemplate($template, $values = []) {
        foreach ($values as $key => $value) {
            $this->f3->set($key, $value);
        }
        $this->msg = \Template::instance()->render($template);
        foreach ($values as $key => $value) {
            $this->f3->clear($key);
        }
        return $this;
    }

    /**
     * Send the mail.
     *
     * @param bool $log toggle log of the client-server smtp conversation
     * @return Smtp $this Smtp object.
     */
    public function send($log = FALSE) {
        $this->smtp->send($this->msg, $log);
        return $this;
    }

    /**
     * Get the underling \SMTP object.
     *
     * @return \SMTP
     */
    public function get() {
        return $this->smtp;
    }

}
