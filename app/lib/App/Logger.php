<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2017 filips
 * Copyright (c) 2017-2017 kumy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

namespace App;

/**
 * Class Logger
 *
 * @package App
 */
abstract class Logger extends \Prefab {

    /**
     * @var \Base $f3
     */
    protected $f3;
    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * Logger constructor.
     */
    public function __construct() {
        $this->f3 = \Base::instance();
    }

    /**
     * Return base log message.
     *
     * @return string Log message.
     */
    private function header() {
        $user = $this->f3->get('SESSION.user');
        $userid = $this->f3->get('SESSION.userid');
        $page = $this->f3->get('PARAMS.0');
        if (empty($userid)) {
            $user = 'Anonymous';
            $userid = '0';
        }
        return "userid:$userid $user - page:$page || ";
    }

    /**
     * Log a PDO exception with headers.
     *
     * @param \PDOException $e PDO Exception.
     * @return mixed
     */
    static public function exception($e) {
        $elog = self::instance();
        $header = $elog->header();
        $info = $e->errorInfo;
        return $elog->logger->write($header.$info[0].' - '.$info[2]);
    }

    /**
     * Log a custom message with headers.
     *
     * @param string $message Message.
     * @return mixed
     */
    static public function message($message) {
        $elog = self::instance();
        $header = $elog->header();
        return $elog->logger->write($header.$message);
    }

}
