FROM php:7.1-apache

MAINTAINER Mathieu Alorent <contact@geokretymap.org>

# Add extension to php
RUN apt-get update \
    && apt-get install -y \
 	libmagickwand-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        graphicsmagick-imagemagick-compat \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

RUN docker-php-ext-install gettext mysqli mcrypt pdo_mysql \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && a2enmod rewrite \

    # From docker image has some problem with CA bundle not defined in php
    && echo "openssl.cafile=/etc/ssl/certs/ca-certificates.crt" > /usr/local/etc/php/conf.d/openssl-ca.ini \
    && echo 'date.timezone = "Europe/Paris"' > /usr/local/etc/php/conf.d/timezone.ini

## Disabled at the moment.
#COPY * /var/www/

# Use it with: docker run -it --rm --name geokrety -p 80:80 -v $(pwd):/var/www/ -v /etc/localtime:/etc/localtime geokrety


#  geokrety:
#    image: geokrety
#    ports:
#      - 80:80
#    volumes:
#      - /etc/localtime:/etc/localtime
