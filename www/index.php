<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('GMT');
}

chdir(__DIR__ . '/../app');

// Initialize code codeverage
include (__DIR__ . '/../c3.php');

// composer autoloader for required packages and dependencies
require_once(__DIR__ . '/../vendor/autoload.php');

// Kickstart the framework
$f3 = \Base::instance();

// Initialize CMS
$f3->config('../app/config/default.ini');

// Initialise with custom values
$f3->config('../app/config/config.ini');

// Initialise with custom values
$f3->config('../app/config/unittest.ini');

// Define routes
$f3->config('../app/config/routes.ini');

// Pretty error handling
//Falsum\Run::handler();

// Define extends
\Template::instance()->extend('logo','\App\Helpers\LogoImage::render');
\Template::instance()->extend('user','\App\Helpers\User::render');
\Template::instance()->extend('flagicon','\App\Helpers\FlagIcon::render');
\Template::instance()->extend('typeicon','\App\Helpers\GeokretType::render');
\Template::instance()->extend('logtypeicon','\App\Helpers\LogType::render');
\Template::instance()->extend('imagetooltip','\App\Helpers\ToolTip::render');
\Template::instance()->extend('gkname','\App\Helpers\GeokretName::render');
\Template::instance()->extend('gklink','\App\Helpers\GeokretLink::render');
\Template::instance()->extend('waypoint','\App\Helpers\WaypointLink::render');
\Template::instance()->extend('page','\App\Helpers\Pagination::render');
\Template::instance()->extend('medals','\App\Helpers\Medals::render');
\Template::instance()->extend('badge','\App\Helpers\Badge::render');

\Template::instance()->extend('pagebrowser','\Pagination::renderTag');
\Template::instance()->filter('error','\App\Helpers\ErrorForm::printErrors');

// TODO LANGUAGE from user preferences

// Load lang from cookie
if ($f3->exists('COOKIE.lang')) {
    $f3->set('LANGUAGE', $f3->get('COOKIE.lang'));
}

// Set lang from url
if ($f3->exists('GET.lang')) {
    $f3->set('LANGUAGE', $f3->get('GET.lang'));
    $f3->set('COOKIE.lang', $f3->get('GET.lang'), 86400); // Save lang in cookie
}

// Execute application
$f3->run();
